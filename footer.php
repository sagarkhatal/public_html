<!-- footer section -->
<footer>
<div class="container-fluid footer" style="background-color:#303C4B;">
    <!-- py for padding along y-axis (top and bottom) -->
        <div class="row py-4 px-5">
            <div class="col-md-4 pl-5">
                
                <img src="./assets/images/logo.png" alt="footer logo" height="100px" width="100px">
                <p class="mt-3" style="color: rgb(211, 204, 204)">
                Growth Enhancer Action of Yashtimadhu increases fertilization rate and embryonic development of foetus. 
                </p>
                
                <ul class="list-unstyled mb-1">
                    <li>
                        <a href="https://www.facebook.com/" target="_blank" class="footer-hover">
                            <i class="fa fa-facebook-square" aria-hidden="true" > Facebook</i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/" target="_blank" class="footer-hover">
                            <i class="fa fa-instagram" aria-hidden="true" > Instagram</i>
                        </a>
                    </li>
            
                </ul>
              
            </div>
            <div class="col-md-4 pl-5">
                <!-- Links -->
                <h5 class="text-uppercase" style="color: #fff;">Quick Links</h5>
                </br>
                <ul class="list-unstyled">
                    <li>
                        <a href="./index.php" class="footer-hover">Home</a>
                    </li>
                    
                    <li>
                        <a href="#aboutus" onclick="showAboutUs()" class="footer-hover">About Us</a>
                    </li>
                    
                     <li>
                        <a href="#contactus" onclick="showContactUs()" class="footer-hover">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4  pl-5">
                <!-- Links -->
                <h5 class="text-uppercase" style="color: #fff;">Contact Us</h5>
                </br>
                <ul class="list-unstyled">
                    <li>
                        <i class="fa fa-map-marker footer-hover" aria-hidden="true" > 48A, Pune - Satara Rd, Adinath Society, Parvati Industrial Estate, Opposite, Pune, Maharashtra 411009</i>
                        
                    </li>
                    <li>
                        <i class="fa fa-phone footer-hover" aria-hidden="true" > +91 98220 28065</i>
                        
                    </li>
                    <li>
                        <i class="fa fa-envelope footer-hover" aria-hidden="true" > imfo@matrujayurveda.com</i>
                    </li>

                </ul>

               
            </div>
        </div>

    <!-- copyright section -->
    <div class="row copyright" style="background-color:#1C242D;">
        <div class="col-4"></div>
        <div class="col-4 text-center">
            <p class="mt-2 mb-2" style="color: rgb(211, 204, 204) ">Copyright 2024 | All Rights Reserved - <a href ="https://www.google.com/maps/place/SKSOFT Solutions+Solutions/@18.4382452,73.8840807,17z/data=!3m1!4b1!4m6!3m5!1s0x3bc2eb4c4a7cdf19:0xfe4dfc7009701446!8m2!3d18.4382401!4d73.8866556!16s%2Fg%2F11sqs2pqjv?entry=ttu" target="_blank"> SKSOFT Solutions</a></p>
        </div>
        <div class="col-4"></div>

    </div>
</div>
</footer>
   