﻿

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <title>Matruj | Jeevan-Shatavarin </title>

        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />

    <meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/32.png" /><link rel="shortcut icon" href="assets/img/72.png" /><link rel="shortcut icon" href="assets/img/114.png" /><link rel="shortcut icon" href="assets/img/144.png" />
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" /><link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet" /><link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:300,400,500,700,400italic,700italic" /><link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" /><link href="assets/css/hover-dropdown-menu.css" rel="stylesheet" />
        <!-- Icomoon Icons -->
        <link href="assets/css/icons.css" rel="stylesheet" />
        <!-- Revolution Slider -->
        <link href="assets/css/revolution-slider.css" rel="stylesheet" /><link href="assets/rs-plugin/css/settings.css" rel="stylesheet" />
        <!-- Animations -->
        <link href="css/animate.min.css" rel="stylesheet" />
        <!-- Owl Carousel Slider -->
        <link href="assets/css/owl/owl.carousel.css" rel="stylesheet" /><link href="assets/css/owl/owl.theme.css" rel="stylesheet" /><link href="assets/css/owl/owl.transitions.css" rel="stylesheet" />
        <!-- PrettyPhoto Popup -->
        <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
        <!-- Custom Style -->
        <link href="assets/css/style.css" rel="stylesheet" /><link href="assets/css/responsive.css" rel="stylesheet" />
        <!-- Color Scheme -->
        <link href="assets/css/color.css" rel="stylesheet" /><link href="assets/css/StyleSheet.css" rel="stylesheet" />
    <style>
        #toTop {
            position: fixed;
            bottom: 10px;
            right: 10px;
            cursor: pointer;
            display: none;
        }
    </style>
<title>

</title></head>
      <div id="pageloader">
            <div class="loader-item fa fa-spin text-color"></div>
        </div>
        <!-- Top Bar -->
    <div class="transparent-header">
			<!-- Sticky Navbar -->
			<header id="sticker" class="sticky-navigation">			
				<!-- Sticky Menu -->
				<div class="navbar navbar-default navbar-bg-light" role="navigation">
					<div class="sticky-menu relative">
						<div class="container">
							<div class="row">
								<div class="col-md-12 pdlfrt">
									<div class="navbar-header mrlft">
									<!-- Button For Responsive toggle -->
								
									<!-- Logo -->
								
									<a class="navbar-brand" href="index.php">
										<img class="site_logo imgsz" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
                                     <span class="mobtogl" style="" id="menu" onclick="openNav()">&#9776; </span>
									<a class="navbar-brand sticky-logo" href="index.php">
										<img class="site_logo imgsz sktp" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
									</div>
								
									<div class="">
									
										<ul class="nav navbar-nav menufnt" style="margin-top:1%;">
											<!-- Home  Mega Menu -->
											<li>
												<a class="active brdr-right mrt" href="index.php">Home</a>
												
											</li>
										    <li>
												<a class="brdr-right mrt" href="about-us.php">About</a>
												
											</li>
                                       
                                         
                                    	<li class="mega-menu">
                                            <a href="product-page.php"" class="brdr-right mrt">Products &nbsp;<i class="fa fa-angle-down" style="font-size:18px"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                   
                                                    <div class="row">
                                                      
                                                        <div class="col-sm-4" style="border-right:1px solid #d2d2d2">
                                                           
                                                            <div class="">
                                                                <div>
                                                                    <a href="pregnancy-care.php" class="parabt prcr anchr active">Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="post-pregnancy-care.php" id="pprgncr" class="parabt psrcr anchr">Post Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="healthcare.php" id="hc" class="parabt htcr anchr">Health Care</a>
                                                                </div>
                                                             
                                                              
                                                            </div>
                                                        </div>
                                                      
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="prcr1">
                                                               
                                                                <div>
                                                                    <a href="Jeevan-shatavarin.php" id="jvnst" class="parabt grey1 anchr">Jeevan Shatavarin</a>
                                                                </div>
                                                               
                                                              
                                                            </div>

                                                            <div class="psrcr1">
                                                                <div>
                                                                    <a href="jeevan-shatavari-plus.php" id="jvnstplus" class="parabt grey1 anchr">Jeevan Shatavarin Plus</a>
                                                                </div>
                                                            </div>

                                                            <div class="htcr1">
                                                                <div>
                                                                    <a href="haldiwala-doodh.php" id="hwd" class="parabt grey1 anchr">Haldiwala Doodh</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="page-links">
                                          
                                                                <img src="assets/img/menu-imgs/Matruj-pc-menu.jpg" class="prgncr" />
                                                                <img src="assets/img/menu-imgs/Matruj-js-menu.jpg" class="jvnst" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-ppc-menu.jpg" class="pprgncr" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-jsp-menu.jpg" class="jvnstplus" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hc-menu.jpg" class="hc" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hd-menu.jpg" class="hwd" style="display:none"/>
                                                        

                                                            </div>
                                                        </div>
                                                 
                                                    </div>
                                                   
                                                </li>
                                            </ul>
                                        </li>
									
                                          
										
                                            <li>
												<a class="mrt" href="Matruj-Contact.php">Contact</a>
												
											</li>
										
										</ul>
									
									</div>
                                     <div id="mySidenav" class="sidenav">
                                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        <div class="overlay-content">
                                            <a href="index.php" onclick="closeNav()">Home</a>
                                            <a href="about-us.php" onclick="closeNav()">About</a>
                                            <a href="product-page.php" onclick="closeNav()">Products</a>
                                              <a href="Matruj-Contact.php" onclick="closeNav()">Contact</a>
                                        </div>
                                    </div>
									
								</div>
								
							</div>
							
						</div>
					
					</div>
				</div>
			</header>
		</div>
<body>
    <form method="post" action="./Jeevan-shatavarin.php" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X8zB+gPm00L5DN1sJ2pIYOMo2RhE3775IZwd4RvmCnNcimIrgARnUHD0rCridGYADtDAQSCmPXM6QmypOJQjSBjG/R4nH033EBIPmipTdJ4=" />
</div>

    <div>
        
        <div class="page-wrap">
            <section class="jshead">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-text">
                                <h1 class="entry-title text-left text-white">Jeevan Shatavarin</h1>
                                <ol class="breadcrumb">
                                    <li><a href="index.php" class="text-white">Home</a></li>
                                    <li class="text-white">Pregnancy Care</li>
                                    <li class="active text-white">Jeevan Shatavarin</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="page-section" style="position:relative;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 top-pad-10">
                            <h2 class="heading hdfnt">Jeevan Shatavarin</h2>
                            <center><img src="assets/img/Matruj-icon-flower.png" /></center>

                        </div>

                        <div class="col-md-12 padjevn">
                            <ul class="carouselnw">
                                <li class="itemsnw main-pos" id="1">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-1.png" />
                                </li>
                                <li class="itemsnw right-pos" id="2">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-2.png" />
                                </li>
                                <li class="itemsnw back-pos" id="3">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-3.png" />
                                </li>
                                <li class="itemsnw back-pos" id="4">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-4.png" />
                                </li>
                                <li class="itemsnw back-pos" id="5">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-5.png" />
                                </li>
                                <li class="itemsnw back-pos" id="6">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-6.png" />
                                </li>
                                <li class="itemsnw back-pos" id="7">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-7.png" />
                                </li>
                                <li class="itemsnw back-pos" id="8">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-8.png" />
                                </li>
                                <li class="itemsnw back-pos" id="9">
                                    <img src="assets/img/jeevan-shatavarin/BOX-JPEG-9.png" />
                                </li>

                            </ul>
                            <span class="arrows" style="display:none">
                                  <input type="button" value="Prev" id="prevnw" />
                                  <input type="button" value="Next" id="nextnw" />
                             </span>
                        </div>

                    </div>
                </div>
            </section>

            <section class="page-section">
                <div class="col-md-12 deskjevn" id="month1">
                    <h4 class="text-center subfnt hdmnt1">Month 1 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_1" data-toggle="tab"> 
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_2" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_3" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_4" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_1">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey fntwt">
                                                “Saponin Glycoside A4” of Shatavari shows strong Anti- Abortive Action.
                                            </p>
                                            <p class="subfnt grey">From enhancing follicullogenesis and ovulation , helping implantation, facilitating growth of foetus to increasing the foetal's weight Shatavari plays a key role in pregnancy.</p>
                                            <p class="subfnt grey fntwt">Growth Enhancer Action of Yashtimadhu increases fertilization rate and embryonic development of foetus.</p>
                                            <p class="subfnt grey">Yashtimadhu helps <b class="fntwt"> in implantation</b> , lubrication and healthy functioning of mucus membrane including cervical mucus, reduces burning sensation and nausea, reduces chances of <b class="fntwt">ectopic pregnancy</b> and <b class="fntwt">chromosomal abnormalities</b> and also acts as strong anti- inflammatory agent. Yashtimadhu shows anti – viral action against HIV,Hepatitis c,<b class="fntwt"> Human Cytomegalo Viraus,Herpes Virus.</b>Yashtimadhu also shows anti- fungal action against candida albicans , anti- fungal action and anti- bacterial action against Salmonella typhii,Staphyloccoccus , Bacillus , E.coli.</p>
                                            <p class="subfnt grey">Shaakbeej contains 15 types of proteins that give nutrition to foetus and also helps <b class="fntwt">in implantation</b>. Shaakbeej ,Shatavari act as rich source of folic acid, calcium, Fe, P,Mg, Vit A,Vit B,Vit E.</p>
                                            <p class="subfnt grey"> Ksheerkakoli , Mudgaparni, Maashparni , Jeevanti act as balya <b class="fntwt">(बल्य)</b> , bruhan <b class="fntwt">(बृहंण )</b>,rasayan <b class="fntwt">( रसायन )</b>, Jeevaneeya <b class="fntwt">(जीवनीय )</b> that impart strength and longevity to foetus.</p>
                                            <p class="subfnt grey"><i class="fntwt"> Devdaru</i> shows anti bacterial, anti fungal, anti tubercular acitivity. Devdaru also shows anti- diabetic action and is strong antioxidant. “Himachalol”, a major constituent of devdaru which shows <b class="fntwt">antispasmodic activity</b>.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_2">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Yashtimadhu</td>
                                                <td>Glycyrrhiza glabra</td>
                                                <td>0.416gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shaakbeej</td>
                                                <td>Tectona grandis</td>
                                                <td>0.416gm</td>
                                            </tr>
                                            <tr>
                                                <td>Ksheerkakoli</td>
                                                <td>Lilium polyphyllum</td>
                                                <td>0.416gm</td>
                                            </tr>

                                            <tr>
                                                <td>Devadaru</td>
                                                <td>Cedrus deodara</td>
                                                <td>0.416gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>
                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadenia reticulate</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_3">

                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-1.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_4">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 1st month of pregnancy.</p>
                                        </center>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month2">
                    <h4 class="text-center subfnt hdmnt2">Month 2 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_21" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_22" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_23" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_24" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_21">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                The <i class="fntwt"> isoflavons</i> specially <i class="fntwt">racemofuran ,asparagamine</i> A and racemosol are responsible for anti- oxidant properties of <i class="fntwt"> Shatavari</i>.<b class="fntwt"> Apart from strong Anti – Abortive Agent - Shatavari also acts as strong Anti- Inflammatory agent</b> from helping implantation, facilitating growth of foetus to increasing fetal weight Shatavari plays key role in pregnancy as it contains Vit A, Vit B1, B2, C, E, Mg, P, Ca, Fe and Folic acid.
                                            </p>
                                            <p class="subfnt grey fntwt"><i class="fntwt">Krishna Til</i> rich source of folic acid which helps in preventing neural tube defects. Rich source of calcium for fetal nutrition. Sessamin And Sessamolin are 2 important muscle protectors that are present in Krishnatil.</p>
                                            <p class="subfnt grey">Krishna Til prevents blood clot formation, ensures healthy growth of fetal brain, also overall growth of foetus and prevents miscarraige.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Manjishta</i> acts as an anti- inflammatory agent, it also shows Anti- bacterial action against G+ve and G-ve strains. <i class="fntwt">Rubiadin</i> of Rubia cordifolia ( means Manjishta ) possess <i class="fntwt">potent </i> antioxidant property. Manjishta acts as Anti-Platelet Activating factor thus prevents abortion. Manjishta also possess Anti-stress Effect which is beneficial during pregnancy.</p>
                                            <p class="subfnt grey">*Mudgaparni – Acts as Anti – inflammatory agent, arrests bleeding , relieves burning sensation or acidity in pregnancy. Also, Mudgaparni has strong hepatoprotective effect on body and it is strong antioxidant agent too. Rich source of vitamin K, Vit C and proteins which nourishes the foetus.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Maashparni </i> nourishes foetus, arrests bleeding, Improves vigor thus giving strength, vitality and longevity to the foetus.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Jeevanti</i>, act as strong Anti Abortive Agent , also shows vasodilation and Anti-depressant effect .Bruhan, Balya, Jeevaneeya as per Ayurveda Jeevanti. Nourishes, strengthens the foetus thus giving longevity and vitality to pregnancy.</p>
                                            <p class="subfnt grey"> Jeevanti provides nutrition to foetus as it is rich in protein, Mg, Ca and Fe. A good rejuvenator and Anti Cancer property also shown by Jeevanti.</p>
                                            <p class="subfnt grey">Jeevanti acts as analgesic relieving pain Jeevanti also protects liver and respiratory system during pregnancy.</p>
                                            <p class="subfnt grey"><i class="fntwt">Pashanbhed</i> act as Anti inflammatory , diuretic, Anti-microbial cleanses urinary bladder and controls infection.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_22">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Pashanbhed</td>
                                                <td>Bergenia ligulata</td>
                                                <td>0.7gm</td>
                                            </tr>
                                            <tr>
                                                <td>Krishna-til</td>
                                                <td>Sesamum indicum</td>
                                                <td>0.7gm</td>
                                            </tr>
                                            <tr>
                                                <td>Manjishta</td>
                                                <td>Rubia cordifolia</td>
                                                <td>0.7gm</td>
                                            </tr>

                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>
                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>
                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadenia reticulata</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_23">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-2.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_24">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 2nd month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month3">
                    <h4 class="text-center subfnt hdmnt3">Month 3 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_31" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_32" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_33" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_34" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_31">

                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                Apart from Shatavari, Ksheerkakoli , Mudgaparni, Maashparni and Jeevanti, here are the 3rd month special herbs -
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Bandak </i> possess strong Anti – Abortive Action . Also the muscle relaxant property of Bandak helps to reduce the pain in uterine muscles during weight bearing stage of pregnancy.</p>
                                            <p class="subfnt grey"><i class="fntwt">Manjishta</i> tones of blood vessels, strengthens vascular system and provides natural nutrition to genitals.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Nilofer </i>Nymphayol – a constituent of Nilofer shows anti- diabetic property. Reverses damaged endocrine tissue and stimulates secretion of insulin in beta cells. Prevents gestational diabetes, also shows cardio protective action, Antipyretic ,hepatoprotective , anti inflammatory action and controls infection.</p>

                                            <p class="subfnt grey"><i class="fntwt">Sariva</i>, prevents chromosomal abnormalities. Sariva acts as a memory enhancer, improves mood and de-stresses the body. Strengthens uterine muscles thereby preventing miscarriage. Imparts skin glow, Anti – Allergic, anti-oxidant action of Sariva is helpful. As per Ayurveda, Sariva is Raktaprasadak which builds up blood vascular system and also facilitates healthy growth of placenta.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_default_32">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Bandak</td>
                                                <td>Dendrophthoe falcata</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Ksheerkakoli</td>
                                                <td>Lilium polyphyllum</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Manjishta</td>
                                                <td>Rubia cordifolia</td>
                                                <td>0.33gm</td>
                                            </tr>

                                            <tr>
                                                <td>Nilofar</td>
                                                <td>Nymphaea nouchali</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Sariva</td>
                                                <td>Hemidesmus indicus</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>

                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadenia reticulata</td>
                                                <td>0.14gm</td>
                                            </tr>
                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_33">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-3.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_34">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 3rd month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month4">
                    <h4 class="text-center subfnt hdmnt4">Month 4 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_41" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_42" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_43" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_44" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_41">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Yashtimadhu</i>, the cortisol – like effect of yashtimadhu support healthy blood sugar levels thus controlling gestational diabetes. Yashtimadhu with strong anti- inflammatory property useful in bacterial vaginosis thereby preventing infection to birth canal, Enhances healthy mucus membrane functioning thereby improving lung function, checks acidity and achieves lubrication of cervical passage.
                                            </p>
                                            <p class="subfnt grey">Sariva , Durlabha ,Yashtimadhu- together help develop blood vascular system , helps to complete the formation and efficient functioning of placenta and also improve immunogloblins functioning ,all collectively result in healthy foetus growth and development.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Rasna</i>, acts as best analgesic , muscle relaxant.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Bharangi</i>, shows Anti cancer , anti-bacterial, Anti – oxidant , Hepatoprotective activity. Strong anti – allergic and lung protector property is useful for pregnancy.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_default_42">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Sariva</td>
                                                <td>Hemidesmus indicus</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Durlabha</td>
                                                <td>Cryptolepis buchanani</td>
                                                <td>0.25gm</td>
                                            </tr>
                                            <tr>
                                                <td>Rasna</td>
                                                <td>Pluchea lanceolata</td>
                                                <td>0.25gm</td>
                                            </tr>

                                            <tr>
                                                <td>Bharangi</td>
                                                <td>Clerodendrum serratum</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Yashtimadhu</td>
                                                <td>Glycyrrhiza glabra</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>

                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadenia reticulata</td>
                                                <td>0.14gm</td>
                                            </tr>
                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_43">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-4.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.
                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_44">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 4th month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month5">
                    <h4 class="text-center subfnt hdmnt5">Month 5 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_51" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_52" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_53" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_54" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_51">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Kantakari ,Bruhati</i> contains Ca,K,Na,Zn and Fe which gives nutrition to the foetus in the womb and also prevents Gram positive and Gram negative bacterial infection. Also, it increase uterine weight.
                                            </p>
                                            <p class="subfnt grey"> <i class="fntwt">Gambhari</i>, increases immune power, tones up mammary glands , along with shatavari preparation for lactation is achieved. Reduces tendency of haemorrhoides or piles in pregnancy.</p>
                                            <p class="subfnt grey"><i class="fntwt">Vatankur</i>, this herb is rich in tanins which stabilizes cervical os by strengthening uterine muscles thus prevent cervical incompetence .As per Ayurveda it is called Gabhastapak (retains pregnancy) and prevents abortion. Vatankur shows strong styptic action helping in stoppage of bleeding due to threatened abortion.</p>

                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_52">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Kantakari</td>
                                                <td>Solanum surattense</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Bruhati</td>
                                                <td>Solanum indicum</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Gambhari</td>
                                                <td>Gmelina arborea</td>
                                                <td>0.4gm</td>
                                            </tr>

                                            <tr>
                                                <td>Vata</td>
                                                <td>Ficus Bengalensis</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>

                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadenia reticulata</td>
                                                <td>0.14gm</td>
                                            </tr>
                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_53">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-5.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_54">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 5th month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month6">
                    <h4 class="text-center subfnt hdmnt6">Month 6 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_61" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_62" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_63" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_64" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_61">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Gokshur</i> along with Shatavari - acts as an anti hypertensive , diuretic and shows anti ADH activity, thereby effectively controlling albuminuria, pedal odema and reducing chances of Eclampsia. Thus Gokshur helps in Pregnancy Induced Hypertension Muscle development of foetus achieved by “Protodiosin” of Gokshur along with bala and shatavari.
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Bala</i> provides strength to uterus as well as foetus, prevents odema and controls cervical pain.</p>
                                            <p class="subfnt grey"><i class="fntwt">Shigru</i> best analgesic , rich source of proteins, Vit B, Fe and Calcium provides nutrition to foetus.</p>
                                            <p class="subfnt grey"><i class="fntwt">Prushniparni</i> protects kidney, maintains B.P, Serum Creatinine. Best Anti-bacterial action shown by prushniparni.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_62">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Prushniparni</td>
                                                <td>Uraria picta</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Bala</td>
                                                <td>Sida cordifolia</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shigru</td>
                                                <td>Moringa oleifera</td>
                                                <td>0.33gm</td>
                                            </tr>

                                            <tr>
                                                <td>Gokshura</td>
                                                <td>Tribulus terrestris</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Gambhari</td>
                                                <td>Gmelina arborea</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>
                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadenia reticulata</td>
                                                <td>0.14gm</td>
                                            </tr>
                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_63">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-6.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_64">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 6th month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month7">
                    <h4 class="text-center subfnt hdmnt7">Month 7 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_71" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_72" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_73" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_74" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_71">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Singhada </i> provides rich nutritional source for growing foetus as it contains Vit B complex (thiamine, riboflavin, pantothenic acid, nicotinic acid, pyridoxine) Vit A,Vit C, D - amylase, amylase and phosphorylase. Also, prepares mother’s body to withstand stress of pregnancy.
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Kamal</i> acts as an excellent source of Vit C and powerful water soluble antioxidant which is essential in collagen synthesis maintaining integrity of blood vessels, skin, organs and bones.</p>
                                            <p class="subfnt grey"><i class="fntwt">Manuka </i> rich in Vit C, Vit A, B1, B2, B6 and folic acid helps in neural growth</p>
                                            <p class="subfnt grey"><i class="fntwt">Kasheru </i> prevents abortion and helps in maintainance of pregnancy.</p>
                                            <p class="subfnt grey"><i class="fntwt">Yashtimadhu</i> acts as a strong growth enhancer and helps in achieving Eutocia ( full term pregnancy) and reduce chances of pre- mature delivery.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_72">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Singhada</td>
                                                <td>Trapa natans</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Kamal </td>
                                                <td>Nelumbo nucifera</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Manuka</td>
                                                <td>Vitis vinifera</td>
                                                <td>0.33gm</td>
                                            </tr>

                                            <tr>
                                                <td>Kasheru</td>
                                                <td>Scirpus grossus</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Yashtimadhu</td>
                                                <td>Glycerrhiza glabra</td>
                                                <td>0.33gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>
                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadenia reticulata</td>
                                                <td>0.14gm</td>
                                            </tr>
                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_73">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-7.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_74">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 7th month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month8">
                    <h4 class="text-center subfnt hdmnt8">Month 8 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_81" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_82" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_83" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_84" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_81">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Bruhati and Kantakari</i>, these herbs contain phytosterols which supplements in progressing the lung development of foetus.
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Ikshu </i>, possess diuretic property which helps in Eclampsia like conditions. Best lactogenic action. Possess anti bacterial action and has cardioprotective , respiratory protective action.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Bilwa </i>, acts as body Immune System Enhancer and imparts strength for normal delivery. </p>
                                            <p class="subfnt grey"><i class="fntwt">Shatavari, Mudgaparni , Mashparni , Jeevanti</i> acts as balya (बल्य) , bruhan (बृहंण ),rasayan ( रसायन ), Jeevaneeya(जीवनीय ) that imparts strength and longevity to foetus.</p>
                                            <p class="subfnt grey"><i class="fntwt">Patol </i>, reduces chance of gestational diabetes.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_82">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Khadir</td>
                                                <td>Acasia catechu</td>
                                                <td>0.28gm</td>
                                            </tr>
                                            <tr>
                                                <td>Bilva </td>
                                                <td>Aegle marmelos</td>
                                                <td>0.28gm</td>
                                            </tr>
                                            <tr>
                                                <td>Bruhati</td>
                                                <td> Solanum indicum</td>
                                                <td>0.28gm</td>
                                            </tr>

                                            <tr>
                                                <td>Patol</td>
                                                <td>Tricosanthes dioica</td>
                                                <td>0.28gm</td>
                                            </tr>
                                            <tr>
                                                <td>Ikshumul</td>
                                                <td>Saccharum officinarum</td>
                                                <td>0.28gm</td>
                                            </tr>
                                            <tr>
                                                <td>Kantakari</td>
                                                <td>Solanum surattense</td>
                                                <td>0.28gm</td>
                                            </tr>
                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>
                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_83">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-8.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_84">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 8th month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 deskjevn" id="month9">
                    <h4 class="text-center subfnt hdmnt9">Month 9 Special</h4>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_91" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_92" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_93" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_94" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_91">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt"> Shatavari </i> The carrying mother is blessed by best ante-natal tonic Shatavari that helps in all stages of pregnancy from implantation to full term stage, providing nutrition to foetus ,achieving healthy growth of foetus and healthy natural delivery of the baby .
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Yashtimadhu, Durlabha, Sariva </i> strengthen uterine muscles and improves respiratory function.</p>
                                            <p class="subfnt grey fntwt fntadv"> Advantages of consuming Jeevan Shatavarin throughout 9 months are –</p>
                                            <ul class="subfnt grey lfmarg">
                                                <li class="indctli">Softening of pelvis , waist and back. </li>
                                                <li class="indctli">Downword movement of “ Vata ”, needed for normal expulsion of foetus at the time of delivery.</li>
                                                <li class="indctli">Promotion of strength and complexion.</li>
                                                <li class="indctli">Normalisation of urine and stool. </li>
                                                <li class="indctli">Delivery with ease of healthy and fully developed child at proper time.</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_92">

                                    <table class="table table-bordered subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Yashtimadhu</td>
                                                <td>Glycerrhiza glabra</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Durlabha </td>
                                                <td>Durlabha</td>
                                                <td>0.4gm</td>
                                            </tr>
                                            <tr>
                                                <td>Ksheerkakoli</td>
                                                <td>Lilium polyphyllum</td>
                                                <td>0.4gm</td>
                                            </tr>

                                            <tr>
                                                <td>Sariva</td>
                                                <td>Hemidesmus indicus</td>
                                                <td>0.4gm</td>
                                            </tr>

                                            <tr>
                                                <td>Shatavari swarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>
                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_93">
                                    <center><img src="assets/img/jeevan-pouches/pouch-mockup-9.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.

                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_94">
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhaposhak</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi listyl" style="padding-left:0px;padding-right:0px">
                                            <h4>Rasayan</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-md-3"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhastravahar</h4></li>
                                        <li class="col-md-1"></li>
                                        <li class="col-indi1 listyl">
                                            <h4>Garbhasthapak</h4></li>
                                        <li class="col-md-3"></li>
                                    </ul>
                                    <div class="col-md-12 tpmrind1 grey">
                                        <center>
                                            <p class="mntspl">Anti-Abortive, Antenatal care-useful in 9th month of pregnancy.</p>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon1">
                    <h4 class="text-center hdmnt1">Month 1 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">
                                        <div class="col-md-12">

                                            <div class="text-left text-justify">
                                                <p class="subfnt grey fntwt">
                                                    “Saponin Glycoside A4” of Shatavari shows strong Anti- Abortive Action.
                                                </p>
                                                <p class="subfnt grey">From enhancing follicullogenesis and ovulation , helping implantation, facilitating growth of foetus to increasing the foetal's weight Shatavari plays a key role in pregnancy.</p>
                                                <p class="subfnt grey fntwt">Growth Enhancer Action of Yashtimadhu increases fertilization rate and embryonic development of foetus.</p>
                                                <p class="subfnt grey">Yashtimadhu helps <b class="fntwt"> in implantation</b> , lubrication and healthy functioning of mucus membrane including cervical mucus, reduces burning sensation and nausea, reduces chances of <b class="fntwt">ectopic pregnancy</b> and <b class="fntwt">chromosomal abnormalities</b> and also acts as strong anti- inflammatory agent. Yashtimadhu shows anti – viral action against HIV,Hepatitis c,<b class="fntwt"> Human Cytomegalo Viraus,Herpes Virus.</b>Yashtimadhu also shows anti- fungal action against candida albicans , anti- fungal action and anti- bacterial action against Salmonella typhii,Staphyloccoccus , Bacillus , E.coli.</p>
                                                <p class="subfnt grey">Shaakbeej contains 15 types of proteins that give nutrition to foetus and also helps <b class="fntwt">in implantation</b>. Shaakbeej ,Shatavari act as rich source of folic acid, calcium, Fe, P,Mg, Vit A,Vit B,Vit E.</p>
                                                <p class="subfnt grey"> Ksheerkakoli , Mudgaparni, Maashparni , Jeevanti act as balya <b class="fntwt">(बल्य)</b> , bruhan <b class="fntwt">(बृहंण )</b>,rasayan <b class="fntwt">( रसायन )</b>, Jeevaneeya <b class="fntwt">(जीवनीय )</b> that impart strength and longevity to foetus.</p>
                                                <p class="subfnt grey"><i class="fntwt"> Devdaru</i> shows anti bacterial, anti fungal, anti tubercular acitivity. Devdaru also shows anti- diabetic action and is strong antioxidant. “Himachalol”, a major constituent of devdaru which shows <b class="fntwt">antispasmodic activity</b>.</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Yashtimadhu</td>
                                                        <td>Glycyrrhiza glabra</td>
                                                        <td>0.416gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shaakbeej</td>
                                                        <td>Tectona grandis</td>
                                                        <td>0.416gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ksheerkakoli</td>
                                                        <td>Lilium polyphyllum</td>
                                                        <td>0.416gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Devadaru</td>
                                                        <td>Cedrus deodara</td>
                                                        <td>0.416gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadenia reticulate</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-1.png" class="pochwid" /></center>
                                        <p>1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> Indications <i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>
                                        </ul>

                                        <div class="col-md-12 top-margin-20 grey">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 1st month of pregnancy.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon2">
                    <h4 class="text-center hdmnt2">Month 2 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion2" href="#collapse21"> Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse21" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">
                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                The <i class="fntwt"> isoflavons</i> specially <i class="fntwt">racemofuran ,asparagamine</i> A and racemosol are responsible for anti- oxidant properties of <i class="fntwt"> Shatavari</i>.<b class="fntwt"> Apart from strong Anti – Abortive Agent - Shatavari also acts as strong Anti- Inflammatory agent</b> from helping implantation, facilitating growth of foetus to increasing fetal weight Shatavari plays key role in pregnancy as it contains Vit A,Vit B1,B2,C,E,Mg,P,Ca,Fe and Folic acid.
                                            </p>
                                            <p class="subfnt grey fntwt"><i class="fntwt">Krishna Til</i> rich source of folic acid which helps in preventing neural tube defects. Rich source of calcium for fetal nutrition. Sessamin And Sessamolin are 2 important muscle protectors that are present in Krishnatil.</p>
                                            <p class="subfnt grey">Krishna Til prevents blood clot formation, ensures healthy growth of fetal brain, also overall growth of foetus and prevents miscarraige.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Manjishta</i> acts as an anti- inflammatory agent, it also shows Anti- bacterial action against G+ve and G-ve strains. <i class="fntwt">Rubiadin</i> of Rubia cordifolia ( means Manjishta ) possess <i class="fntwt">potent </i> antioxidant property. Manjishta acts as Anti-Platelet Activating factor thus prevents abortion. Manjishta also possess Anti-stress Effect which is beneficial during pregnancy.</p>
                                            <p class="subfnt grey">*Mudgaparni – Acts as Anti – inflammatory agent, arrests bleeding , relieves burning sensation or acidity in pregnancy. Also, Mudgaparni has strong hepatoprotective effect on body and it is strong antioxidant agent too. Rich source of vitamin K, Vit C and proteins which nourishes the foetus.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Maashparni </i> nourishes foetus, arrests bleeding, Improves vigor thus giving strength, vitality and longevity to the foetus.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Jeevanti</i>, act as strong Anti Abortive Agent , also shows vasodilation and Anti-depressant effect .Bruhan, Balya, Jeevaneeya as per Ayurveda Jeevanti. Nourishes, strengthens the foetus thus giving longevity and vitality to pregnancy.</p>
                                            <p class="subfnt grey"> Jeevanti provides nutrition to foetus as it is rich in protein, Mg, Ca and Fe. A good rejuvenator and Anti Cancer property also shown by Jeevanti.</p>
                                            <p class="subfnt grey">Jeevanti acts as analgesic relieving pain Jeevanti also protects liver and respiratory system during pregnancy.</p>
                                            <p class="subfnt grey"><i class="fntwt">Pashanbhed</i> act as Anti inflammatory , diuretic, Anti-microbial cleanses urinary bladder and controls infection.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion2" href="#collapse22">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse22" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Pashanbhed</td>
                                                        <td>Bergenia ligulata</td>
                                                        <td>0.7gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Krishna-til</td>
                                                        <td>Sesamum indicum</td>
                                                        <td>0.7gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Manjishta</td>
                                                        <td>Rubia cordifolia</td>
                                                        <td>0.7gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadenia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion2" href="#collapse23">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse23" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-2.png" class="pochwid" /></center>
                                        <p> 1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion2" href="#collapse24">Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse24" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>
                                        </ul>

                                        <div class="col-md-12 top-margin-20 grey">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 2nd month of pregnancy.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon3">
                    <h4 class="text-center hdmnt3">Month 3 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion3" href="#collapse31"> Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse31" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">
                                        <div class="col-md-12">

                                            <div class="text-left text-justify">
                                                <p class="subfnt grey">
                                                    Apart from Shatavari, Ksheerkakoli , Mudgaparni, Maashparni and Jeevanti, here are the 3rd month special herbs -
                                                </p>
                                                <p class="subfnt grey"><i class="fntwt">Bandak </i> possess strong Anti – Abortive Action . Also the muscle relaxant property of Bandak helps to reduce the pain in uterine muscles during weight bearing stage of pregnancy.</p>
                                                <p class="subfnt grey"><i class="fntwt">Manjishta</i> tones of blood vessels, strengthens vascular system and provides natural nutrition to genitals.</p>
                                                <p class="subfnt grey"> <i class="fntwt">Nilofer </i>Nymphayol – a constituent of Nilofer shows anti- diabetic property. Reverses damaged endocrine tissue and stimulates secretion of insulin in beta cells. Prevents gestational diabetes, also shows cardio protective action, Antipyretic ,hepatoprotective , anti inflammatory action and controls infection.</p>

                                                <p class="subfnt grey"><i class="fntwt">Sariva</i>, prevents chromosomal abnormalities. Sariva acts as a memory enhancer, improves mood and de-stresses the body. Strengthens uterine muscles thereby preventing miscarriage. Imparts skin glow, Anti – Allergic, anti-oxidant action of Sariva is helpful. As per Ayurveda, Sariva is Raktaprasadak which builds up blood vascular system and also facilitates healthy growth of placenta.</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion3" href="#collapse32">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse32" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Bandak</td>
                                                        <td>Dendrophthoe falcata</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ksheerkakoli</td>
                                                        <td>Lilium polyphyllum</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Manjishta</td>
                                                        <td>Rubia cordifolia</td>
                                                        <td>0.33gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Nilofar</td>
                                                        <td>Nymphaea nouchali</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sariva</td>
                                                        <td>Hemidesmus indicus</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadenia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion3" href="#collapse33">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse33" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-3.png" class="pochwid" /></center>
                                        <p> 1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion3" href="#collapse34"> Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse34" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>

                                        </ul>
                                        <div class="col-md-12 top-margin-20 grey">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 3rd month of pregnancy.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon4">
                    <h4 class="text-center hdmnt4">Month 4 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion4" href="#collapse41"> Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse41" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">

                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Yashtimadhu</i>, the cortisol – like effect of yashtimadhu support healthy blood sugar levels thus controlling gestational diabetes. Yashtimadhu with strong anti- inflammatory property useful in bacterial vaginosis thereby preventing infection to birth canal, Enhances healthy mucus membrane functioning thereby improving lung function, checks acidity and achieves lubrication of cervical passage.
                                            </p>
                                            <p class="subfnt grey">Sariva , Durlabha ,Yashtimadhu- together help develop blood vascular system , helps to complete the formation and efficient functioning of placenta and also improve immunogloblins functioning ,all collectively result in healthy foetus growth and development.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Rasna</i>, acts as best analgesic , muscle relaxant.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Bharangi</i>, shows Anti cancer , anti-bacterial, Anti – oxidant , Hepatoprotective activity. Strong anti – allergic and lung protector property is useful for pregnancy.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion4" href="#collapse42">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse42" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Sariva</td>
                                                        <td>Hemidesmus indicus</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Durlabha</td>
                                                        <td>Cryptolepis buchanani</td>
                                                        <td>0.25gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rasna</td>
                                                        <td>Pluchea lanceolata</td>
                                                        <td>0.25gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Bharangi</td>
                                                        <td>Clerodendrum serratum</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Yashtimadhu</td>
                                                        <td>Glycyrrhiza glabra</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadenia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion4" href="#collapse43">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse43" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-4.png" class="pochwid" /></center>
                                        <p>1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion4" href="#collapse44">Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse44" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>

                                        </ul>
                                        <div class="col-md-12 top-margin-20 grey">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 4th month of pregnancy.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon5">
                    <h4 class="text-center hdmnt5">Month 5 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion5" href="#collapse51">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse51" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">

                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Kantakari ,Bruhati</i> contains Ca,K,Na,Zn and Fe which gives nutrition to the foetus in the womb and also prevents Gram positive and Gram negative bacterial infection. Also, it increase uterine weight.
                                            </p>
                                            <p class="subfnt grey"> <i class="fntwt">Gambhari</i>, increases immune power, tones up mammary glands , along with shatavari preparation for lactation is achieved. Reduces tendency of haemorrhoides or piles in pregnancy.</p>
                                            <p class="subfnt grey"><i class="fntwt">Vatankur</i>, this herb is rich in tanins which stabilizes cervical os by strengthening uterine muscles thus prevent cervical incompetence .As per Ayurveda it is called Gabhastapak (retains pregnancy) and prevents abortion. Vatankur shows strong styptic action helping in stoppage of bleeding due to threatened abortion.</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion5" href="#collapse52">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse52" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Kantakari</td>
                                                        <td>Solanum surattense</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bruhati</td>
                                                        <td>Solanum indicum</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gambhari</td>
                                                        <td>Gmelina arborea</td>
                                                        <td>0.4gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Vata</td>
                                                        <td>Ficus Bengalensis</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadenia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion5" href="#collapse53">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse53" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-5.png" class="pochwid" /></center>
                                        <p>1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion5" href="#collapse54"> Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse54" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>
                                        </ul>

                                        <div class="col-md-12 top-margin-20">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 5th month of pregnancy.</p>
                                            </center>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon6">
                    <h4 class="text-center hdmnt6">Month 6 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion6" href="#collapse61">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse61" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">

                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Gokshur</i> along with Shatavari - acts as an anti hypertensive , diuretic and shows anti ADH activity, thereby effectively controlling albuminuria, pedal odema and reducing chances of Eclampsia. Thus Gokshur helps in Pregnancy Induced Hypertension Muscle development of foetus achieved by “Protodiosin” of Gokshur along with bala and shatavari.
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Bala</i> provides strength to uterus as well as foetus, prevents odema and controls cervical pain.</p>
                                            <p class="subfnt grey"><i class="fntwt">Shigru</i> best analgesic , rich source of proteins, Vit B, Fe and Calcium provides nutrition to foetus.</p>
                                            <p class="subfnt grey"><i class="fntwt">Prushniparni</i> protects kidney, maintains B.P, Serum Creatinine. Best Anti-bacterial action shown by prushniparni.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion6" href="#collapse62">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse62" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Prushniparni</td>
                                                        <td>Uraria picta</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bala</td>
                                                        <td>Sida cordifolia</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shigru</td>
                                                        <td>Moringa oleifera</td>
                                                        <td>0.33gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Gokshura</td>
                                                        <td>Tribulus terrestris</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gambhari</td>
                                                        <td>Gmelina arborea</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadenia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion6" href="#collapse63">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse63" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-6.png" class="pochwid" /></center>
                                        <p> 1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion6" href="#collapse64"> Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse64" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>

                                        </ul>

                                        <div class="col-md-12 top-margin-20">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 6th month of pregnancy.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon7">
                    <h4 class="text-center hdmnt7">Month 7 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion7">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion7" href="#collapse71">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse71" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">

                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Singhada </i> provides rich nutritional source for growing foetus as it contains Vit B complex (thiamine, riboflavin, pantothenic acid, nicotinic acid, pyridoxine) Vit A,Vit C, D - amylase, amylase and phosphorylase. Also, prepares mother’s body to withstand stress of pregnancy.
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Kamal</i> acts as an excellent source of Vit C and powerful water soluble antioxidant which is essential in collagen synthesis maintaining integrity of blood vessels, skin, organs and bones.</p>
                                            <p class="subfnt grey"><i class="fntwt">Manuka </i> rich in Vit C, Vit A, B1, B2, B6 and folic acid helps in neural growth</p>
                                            <p class="subfnt grey"><i class="fntwt">Kasheru </i> prevents abortion and helps in maintainance of pregnancy.</p>
                                            <p class="subfnt grey"><i class="fntwt">Yashtimadhu</i> acts as a strong growth enhancer and helps in achieving Eutocia ( full term pregnancy) and reduce chances of pre- mature delivery.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion7" href="#collapse72">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse72" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Singhada</td>
                                                        <td>Trapa natans</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kamal </td>
                                                        <td>Nelumbo nucifera</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Manuka</td>
                                                        <td>Vitis vinifera</td>
                                                        <td>0.33gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Kasheru</td>
                                                        <td>Scirpus grossus</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Yashtimadhu</td>
                                                        <td>Glycerrhiza glabra</td>
                                                        <td>0.33gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadenia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion7" href="#collapse73">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse73" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-7.png" class="pochwid" /></center>
                                        <p>1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion7" href="#collapse74">Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse74" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>
                                        </ul>

                                        <div class="col-md-12 top-margin-20">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 7th month of pregnancy.</p>
                                            </center>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon8">
                    <h4 class="text-center hdmnt8">Month 8 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion8" href="#collapse81">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse81" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">

                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt">Bruhati and Kantakari</i>, these herbs contain phytosterols which supplements in progressing the lung development of foetus.
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Ikshu </i>, possess diuretic property which helps in Eclampsia like conditions. Best lactogenic action. Possess anti bacterial action and has cardioprotective , respiratory protective action.</p>
                                            <p class="subfnt grey"> <i class="fntwt">Bilwa </i>, acts as body Immune System Enhancer and imparts strength for normal delivery. </p>
                                            <p class="subfnt grey"><i class="fntwt">Shatavari, Mudgaparni , Mashparni , Jeevanti</i> acts as balya (बल्य) , bruhan (बृहंण ),rasayan ( रसायन ), Jeevaneeya(जीवनीय ) that imparts strength and longevity to foetus.</p>
                                            <p class="subfnt grey"><i class="fntwt">Patol </i>, reduces chance of gestational diabetes.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion8" href="#collapse82">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse82" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Khadir</td>
                                                        <td>Acasia catechu</td>
                                                        <td>0.28gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bilva </td>
                                                        <td>Aegle marmelos</td>
                                                        <td>0.28gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bruhati</td>
                                                        <td> Solanum indicum</td>
                                                        <td>0.28gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Patol</td>
                                                        <td>Tricosanthes dioica</td>
                                                        <td>0.28gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ikshumul</td>
                                                        <td>Saccharum officinarum</td>
                                                        <td>0.28gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kantakari</td>
                                                        <td>Solanum surattense</td>
                                                        <td>0.28gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion8" href="#collapse83">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse83" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-8.png" class="pochwid" /></center>
                                        <p> 1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion8" href="#collapse84"> Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse84" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>
                                        </ul>

                                        <div class="col-md-12 top-margin-20">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 8th month of pregnancy.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container mobjevn" id="mon9">
                    <h4 class="text-center hdmnt9">Month 9 Special</h4>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion9">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion9" href="#collapse91">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse91" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">

                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                <i class="fntwt"> Shatavari </i> The carrying mother is blessed by best ante-natal tonic Shatavari that helps in all stages of pregnancy from implantation to full term stage, providing nutrition to foetus ,achieving healthy growth of foetus and healthy natural delivery of the baby .
                                            </p>
                                            <p class="subfnt grey"><i class="fntwt">Yashtimadhu, Durlabha, Sariva </i> strengthen uterine muscles and improves respiratory function.</p>
                                            <p class="subfnt grey fntwt fntadv"> Advantages of consuming Jeevan Shatavarin throughout 9 months are –</p>
                                            <ul class="subfnt grey lfmarg">
                                                <li class="indctli">Softening of pelvis , waist and back. </li>
                                                <li class="indctli">Downword movement of “ Vata ”, needed for normal expulsion of foetus at the time of delivery.</li>
                                                <li class="indctli">Promotion of strength and complexion.</li>
                                                <li class="indctli">Normalisation of urine and stool. </li>
                                                <li class="indctli">Delivery with ease of healthy and fully developed child at proper time.</li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion9" href="#collapse92">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse92" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Yashtimadhu</td>
                                                        <td>Glycerrhiza glabra</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Durlabha </td>
                                                        <td>Durlabha</td>
                                                        <td>0.4gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ksheerkakoli</td>
                                                        <td>Lilium polyphyllum</td>
                                                        <td>0.4gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Sariva</td>
                                                        <td>Hemidesmus indicus</td>
                                                        <td>0.4gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Shatavari swarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion9" href="#collapse93">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse93" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/jeevan-pouches/pouch-mockup-9.png" class="pochwid" /></center>
                                        <p> 1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion9" href="#collapse94"> Indications<i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse94" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhaposhak</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Rasayan</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhastravahar</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhasthapak</h4></li>

                                        </ul>
                                        <div class="col-md-12 top-margin-20">
                                            <center>
                                                <p class="mntspl">Anti-Abortive, Antenatal care-useful in 9th month of pregnancy.</p>
                                            </center>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            
            <section id="" class="page-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 top-pad-10">
                            <h2 class="heading bottom-margin-50 hdfnt">Other Products</h2>
                            <center><img src="assets/img/Matruj-icon-flower.png" /></center>

                            <div class="tpflw">

                                <div class="prcts">
                                    <div class="col-md-12">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-pad">
                                            <div class="pr1" onclick="window.location.href = 'post-pregnancy-care.php'">
                                                <div class="infonw green">
                                                    <p>Post Pregnancy Care</p>
                                                </div>
                                                <div class="popis1 green">
                                                    <h2 itemprop="name" class="text-center"><a href="post-pregnancy-care.php" class="text-white">Post Pregnancy Care</a></h2>
                                                </div>
                                                <img src="assets/img/Products/Matruj-category2.jpg" class="primg" alt="" />
                                            </div>
                                            <a class="subfnt" href="post-pregnancy-care.php">
                                                <p class="padtx clrtxt2 text-center">Post Pregnancy Care</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-pad">
                                            <div class="pr1" onclick="window.location.href = 'healthcare.php'">
                                                <div class="infonw green">
                                                    <p>Health
                                                        <br /> Care</p>
                                                </div>
                                                <div class="popis1 green">
                                                    <h2 itemprop="name" class="text-center"><a href="healthcare.php" class="text-white">Health Care</a></h2>

                                                </div>
                                                <img src="assets/img/Products/Matruj-category03.jpg" class="primg" alt="" />
                                            </div>
                                            <a class=" subfnt" href="healthcare.php">
                                                <p class="padtx clrtxt2 text-center">Health Care</p>
                                            </a>
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


            
        </div>
    
        <footer id="footer">
            <div class="footer-widget">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                            <img src="assets/img/matruj-logo-footer.png" class="img-responsive"/>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                        <p class="subfnt">
                        <strong class="paratxt">Factory :</strong></p>
                          <p class="subfnt"> S.No. 70/1/1A,
                       Hissa No. 12/10,  <br />Santoshnagar, Lane No. 6,
                        <br />Near PMT Depot,  Katraj<br /> Pune - 411 046<br />
                             
                          </p>
                            <p class="subfnt"> <a href="tel:+919822028065">  +91 9822028065</a></p>
                        <!-- Email -->
                    
                        <!-- Phone -->
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20 padlfcr">
                      <p class="subfnt">
                        <strong class="paratxt">Corporate Office :</strong></p>
                            <p class="subfnt">48A,Parvati Industrial Estate,  <br />
                            Opposite Adinath Society,  <br />
                            Pune - Satara Road, <br />
                            Pune - 411009
                               
                            </p>
                            <p class="subfnt"> <a href="mailto:sales@matrujayurveda.com"> sales@matrujayurveda.com</a></p>
                         </div>
                        <div class="col-md-1"></div>
                        <div class="col-xs-12 col-sm-6 col-md-2 widget bottom-xs-pad-20">
                        
                            <nav class="subfnt">
                                <ul>
                                    <!-- List Items -->
                                    <li class="btmul">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="about-us.php">About</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="product-page.php">Products</a>
                                    </li>
                                 
                                    <li class="btmul">
                                        <a href="Matruj-Contact.php">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        
                    
                    </div>
                    <div class="row top-margin-10">
                        <div class="col-md-12">
                            <p class="text-center fntadv">Matruj Ayurveda Pharmacy Pvt. Ltd.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer-top -->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <!-- Copyrights -->
                       <div class="black col-md-4 col-sm-5 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt"> All rights reserved by Matruj copyright @ 2024 </span>
                       </div>
                         <div class="black col-md-4 col-sm-3 col-xs-12" style="margin-top:10px;float:left" >
                            <center>
                                
                                  <div class="social">
                                      <a href="https://www.facebook.com/matrujayurveda/" target="_blank"><img src="assets/img/footer/matruj-facebook.png" /></a></div>
                                  <div class="social">
                                      <a href="tel:+919822028065"><img src="assets/img/footer/matruj-whatsapp.png" /></a>
                                  </div>
                               </center>
                       </div>
                       <div class="black col-md-4 col-sm-4 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt">Design and Developed by <a href ="https://www.google.com/maps/place/SKSOFT+Solutions/@18.4382452,73.8840807,17z/data=!3m1!4b1!4m6!3m5!1s0x3bc2eb4c4a7cdf19:0xfe4dfc7009701446!8m2!3d18.4382401!4d73.8866556!16s%2Fg%2F11sqs2pqjv?entry=ttu" target="_blank"> SKSOFT Solutions</a></span>
                       </div>
                </div>
                 </div>
             </div>
        
     </footer>

          <!-- Scripts -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script> 
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script> 
    <!-- Menu jQuery plugin -->
     
    <script type="text/javascript" src="assets/js/hover-dropdown-menu.js"></script> 
    <!-- Menu jQuery Bootstrap Addon --> 
    <script type="text/javascript" src="assets/js/jquery.hover-dropdown-menu-addon.js"></script> 
    <!-- Scroll Top Menu -->
     
    <script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script> 
    <!-- Sticky Menu --> 
    <script type="text/javascript" src="assets/js/jquery.sticky.js"></script> 
    <!-- Bootstrap Validation -->
     
    <script type="text/javascript" src="assets/js/bootstrapValidator.min.js"></script> 
    <!-- Revolution Slider -->
     
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
    <script type="text/javascript" src="assets/js/revolution-custom.js"></script> 
    <!-- Portfolio Filter -->
     
    <script type="text/javascript" src="assets/js/jquery.mixitup.min.js"></script> 
    <!-- Animations -->
     
    <script type="text/javascript" src="assets/js/jquery.appear.js"></script> 
    <script type="text/javascript" src="assets/js/effect.js"></script> 
    <!-- Owl Carousel Slider -->
     
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script> 
    <!-- Pretty Photo Popup -->
     
    <script type="text/javascript" src="assets/js/jquery.prettyPhoto.js"></script> 
    <!-- Parallax BG -->
     
    <script type="text/javascript" src="assets/js/jquery.parallax-1.1.3.js"></script> 
    <!-- Fun Factor / Counter -->
     
    <script type="text/javascript" src="assets/js/jquery.countTo.js"></script> 
    <!-- Twitter Feed -->
     
    <script type="text/javascript" src="assets/js/tweet/carousel.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/scripts.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/tweetie.min.js"></script> 
    <!-- Background Video -->
     
    <script type="text/javascript" src="assets/js/jquery.mb.YTPlayer.js"></script> 
    <!-- Custom Js Code -->
     
    <script type="text/javascript" src="assets/js/custom.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="assets/js/JavaScript.js"></script>
        <script>
            $(document).ready(function () {
                $('body').append('<div id="toTop" class="btn topbk"><span class="glyphicon glyphicon-chevron-up"></span></div>');
                $(window).scroll(function () {
                    if ($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }
                });
                $('#toTop').click(function () {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                });
            });

        </script>
        
        <script>
            $('.nav-tabs-dropdown').each(function (i, elm) {

                $(elm).text($(elm).next('ul').find('li.active a').text());

            });

            $('.nav-tabs-dropdown').on('click', function (e) {

                e.preventDefault();

                $(e.target).toggleClass('open').next('ul').slideToggle();

            });

            $('#nav-tabs-wrapper a[data-toggle="tab"]').on('click', function (e) {

                e.preventDefault();

                $(e.target).closest('ul').hide().prev('a').removeClass('open').text($(this).text());

            });

        </script>


    </div>
    
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A928D99A" />
</div></form>

<!-- Visual Studio Browser Link -->
<script type="application/json" id="__browserLink_initializationData">
    {"appName":"Chrome","requestId":"d936fa89467e4b2289684e55b1ea9cb2"}
</script>
<script type="text/javascript" src="http://localhost:64002/4bab1ae8702d47b784f5593e2b6ec05b/browserLink" async="async"></script>
<!-- End Browser Link -->

</body>
</html>
