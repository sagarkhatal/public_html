﻿

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <title>Matruj | Haldiwala Dudh</title>

        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        
        <link href="assets/css/StyleSheet.css" rel="stylesheet" />
    <meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/32.png" /><link rel="shortcut icon" href="assets/img/72.png" /><link rel="shortcut icon" href="assets/img/114.png" /><link rel="shortcut icon" href="assets/img/144.png" />
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" /><link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet" /><link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:300,400,500,700,400italic,700italic" /><link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" /><link href="assets/css/hover-dropdown-menu.css" rel="stylesheet" />
        <!-- Icomoon Icons -->
        <link href="assets/css/icons.css" rel="stylesheet" />
        <!-- Revolution Slider -->
        <link href="assets/css/revolution-slider.css" rel="stylesheet" /><link href="assets/rs-plugin/css/settings.css" rel="stylesheet" />
        <!-- Animations -->
        <link href="css/animate.min.css" rel="stylesheet" />
        <!-- Owl Carousel Slider -->
        <link href="assets/css/owl/owl.carousel.css" rel="stylesheet" /><link href="assets/css/owl/owl.theme.css" rel="stylesheet" /><link href="assets/css/owl/owl.transitions.css" rel="stylesheet" />
        <!-- PrettyPhoto Popup -->
        <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
        <!-- Custom Style -->
        <link href="assets/css/style.css" rel="stylesheet" /><link href="assets/css/responsive.css" rel="stylesheet" />
        <!-- Color Scheme -->
        <link href="assets/css/color.css" rel="stylesheet" /><link href="assets/css/StyleSheet.css" rel="stylesheet" />
    <style>
        #toTop {
            position: fixed;
            bottom: 10px;
            right: 10px;
            cursor: pointer;
            display: none;
        }
    </style>
<title>

</title></head>
      <div id="pageloader">
            <div class="loader-item fa fa-spin text-color"></div>
        </div>
        <!-- Top Bar -->
    <div class="transparent-header">
			<!-- Sticky Navbar -->
			<header id="sticker" class="sticky-navigation">			
				<!-- Sticky Menu -->
				<div class="navbar navbar-default navbar-bg-light" role="navigation">
					<div class="sticky-menu relative">
						<div class="container">
							<div class="row">
								<div class="col-md-12 pdlfrt">
									<div class="navbar-header mrlft">
									<!-- Button For Responsive toggle -->
								
									<!-- Logo -->
								
									<a class="navbar-brand" href="index.php">
										<img class="site_logo imgsz" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
                                     <span class="mobtogl" style="" id="menu" onclick="openNav()">&#9776; </span>
									<a class="navbar-brand sticky-logo" href="index.php">
										<img class="site_logo imgsz sktp" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
									</div>
								
									<div class="">
									
										<ul class="nav navbar-nav menufnt" style="margin-top:1%;">
											<!-- Home  Mega Menu -->
											<li>
												<a class="active brdr-right mrt" href="index.php">Home</a>
												
											</li>
										    <li>
												<a class="brdr-right mrt" href="about-us.php">About</a>
												
											</li>
                                       
                                         
                                    	<li class="mega-menu">
                                            <a href="product-page.php"" class="brdr-right mrt">Products &nbsp;<i class="fa fa-angle-down" style="font-size:18px"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                   
                                                    <div class="row">
                                                      
                                                        <div class="col-sm-4" style="border-right:1px solid #d2d2d2">
                                                           
                                                            <div class="">
                                                                <div>
                                                                    <a href="pregnancy-care.php" class="parabt prcr anchr active">Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="post-pregnancy-care.php" id="pprgncr" class="parabt psrcr anchr">Post Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="healthcare.php" id="hc" class="parabt htcr anchr">Health Care</a>
                                                                </div>
                                                             
                                                              
                                                            </div>
                                                        </div>
                                                      
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="prcr1">
                                                               
                                                                <div>
                                                                    <a href="Jeevan-shatavarin.php" id="jvnst" class="parabt grey1 anchr">Jeevan Shatavarin</a>
                                                                </div>
                                                               
                                                              
                                                            </div>

                                                            <div class="psrcr1">
                                                                <div>
                                                                    <a href="jeevan-shatavari-plus.php" id="jvnstplus" class="parabt grey1 anchr">Jeevan Shatavarin Plus</a>
                                                                </div>
                                                            </div>

                                                            <div class="htcr1">
                                                                <div>
                                                                    <a href="haldiwala-doodh.php" id="hwd" class="parabt grey1 anchr">Haldiwala Doodh</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="page-links">
                                          
                                                                <img src="assets/img/menu-imgs/Matruj-pc-menu.jpg" class="prgncr" />
                                                                <img src="assets/img/menu-imgs/Matruj-js-menu.jpg" class="jvnst" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-ppc-menu.jpg" class="pprgncr" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-jsp-menu.jpg" class="jvnstplus" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hc-menu.jpg" class="hc" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hd-menu.jpg" class="hwd" style="display:none"/>
                                                        

                                                            </div>
                                                        </div>
                                                 
                                                    </div>
                                                   
                                                </li>
                                            </ul>
                                        </li>
									
                                          
										
                                            <li>
												<a class="mrt" href="Matruj-Contact.php">Contact</a>
												
											</li>
										
										</ul>
									
									</div>
                                     <div id="mySidenav" class="sidenav">
                                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        <div class="overlay-content">
                                            <a href="index.php" onclick="closeNav()">Home</a>
                                            <a href="about-us.php" onclick="closeNav()">About</a>
                                            <a href="product-page.php" onclick="closeNav()">Products</a>
                                              <a href="Matruj-Contact.php" onclick="closeNav()">Contact</a>
                                        </div>
                                    </div>
									
								</div>
								
							</div>
							
						</div>
					
					</div>
				</div>
			</header>
		</div>
<body>
    <form method="post" action="./haldiwala-doodh.php" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xiWtL+i9FXA4agw+TEB4VuxYWx/HbvAkD510d5P0ZHoz2FJ917y54U4sjXl9+Gli92YH2m+tWimtnM+m1BU+s+QuKKrtYMPt1Bf60Wf1dUM=" />
</div>

    <div>
        
        <div class="page-wrap">
            <section class="hdhead">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-text">
                                <h1 class="entry-title text-left text-white">Haldiwala Doodh</h1>
                                <ol class="breadcrumb">
                                    <li><a href="index.php" class="text-white">Home</a></li>
                                    <li class="text-white">Health Care</li>
                                    <li class="active text-white">Haldiwala Doodh</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="" style="position:relative;padding:75px 0px 0px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 bottom-pad-50">
                            <h2 class="heading hdfnt">Haldiwala Doodh</h2>
                            <center><img src="assets/img/Matruj-icon-flower.png" /></center>
                        </div>
                        <div class="col-md-12">

                            <center> <img src="assets/img/haldiwala-dudh.png" /></center>
                        </div>
                    </div>
                </div>
            </section>

            <section class="page-section">
                <div class="col-md-12 deskjevn" id="month1">

                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_1" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_2" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_3" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_4" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_1">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <ul class="subfnt grey lfmarg">
                                                <li class="indctli"><b class="fntwt">Wound healing action </b>
                                                    <p><i class="fntwt">Haridra</i> being strong anti- imflammatory, best anti-septic works effectively in wound healing process. Curcumin of haridra produced large infiltration of macrophages, neutrophils and fibroblast also , the rate of formation of granulation tissue is increased.</p>
                                                    <p><i class="fntwt">Yashtimadhu</i> acts as strong anti- imflammatory agent by promoting the healing of ulcers of stomach and mouth.</p>
                                                    <p><i class="fntwt">Kantakari</i> shows anti- oxidant , anti- septic, antimicrobial actions by stimulating interleukin – 8 and also increases tensile strength of collagen fibres thus achieving better wound healing results.</p>
                                                </li>
                                                <li class="indctli"><b class="fntwt">Immunomodulatory action </b>
                                                    <p>Free radicals scavenger action shown by haridra tulsi yashtimadhu .Active principle curcumin downregulates the expression of COX2 , LOX,iNOS, Urosolic acid of Tulsi & Glycerhizin ( Yashtimadhu) show immunomodulatory action.</p>

                                                </li>
                                                <li class="indctli"><b class="fntwt">Digestive action</b>
                                                    <p>Gingerol ,Shagol of Ginger shows strong anti- imflammatory action thereby Stimulating digestion , absorption , also relieves constipation and flatulence ,reduces nausea, vomiting ,<b class="fntwt"> motion sickness</b>. Yashtimadhu shows antifungal activity against candida albicans and is also powerful antibacterial agent against Gram positive and Gram Negative bacteria.</p>

                                                </li>
                                                <li class="indctli"><b class="fntwt">Respiratory care</b>
                                                    <p>Oleonolic acid,Urosolic acid, polyphenolic acid of Tulsi has anti allergic & anti inflammatory effect in cough – cold, sore throat, bronchitis, asthama.Yashtimadhu shows anti tussive activity, anti allergic activity , spasmolytic activity thus reducing respiratory disorders.</p>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_default_2">
                                    <table class="table table-bordered table-responsive subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Yashtimadhu (Glyccerrhiza glabra)</td>
                                                <td>1.00gm</td>
                                            </tr>
                                            <tr>
                                                <td>Kantakari (Solanum xanthocarpum)</td>
                                                <td>1.00gm</td>
                                            </tr>
                                            <tr>
                                                <td>Tulasi Swarasa (Ocimum sanctum)</td>
                                                <td>2.00gm</td>
                                            </tr>

                                            <tr>
                                                <td>Adaraka Swarasa(Zingiber Officinale)</td>
                                                <td>2.00gm</td>
                                            </tr>
                                            <tr>
                                                <td>Haridra(Curcuma longa)</td>
                                                <td>0.28gm</td>
                                            </tr>
                                            <tr>
                                                <td>Sugar</td>
                                                <td>0.50gm</td>

                                            </tr>

                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_3">
                                    <center><img src="assets/img/matruj-hd-pouch.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        10 gms once a day or as directed by the physician.
                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_4">
                                    <ul class="subfnt indctli col-md-12 grey">

                                        <li class="col-indipls3s listyl">
                                            <h4>Best Immuno-modelator</h4></li>

                                        <li class="col-indipls3 listyl">
                                            <h4>Anti-Allergic, Anti-Bacterial.</h4></li>

                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">

                                        <li class="col-indipls3n listyl">
                                            <h4>Relives fever & Flu like symptoms</h4></li>

                                        <li class="col-indipls3nw  listyl">
                                            <h4>Anti-Inflammatory</h4></li>

                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">
                                        <li class="col-indipls3nt listyl">
                                            <h4>Pain relief</h4></li>
                                        <li class="col-indipls3t listyl">
                                            <h4>Wound Healing Enhancer</h4></li>
                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">

                                        <li class="col-indipls3t1  listyl">
                                            <h4>Blood-Purifier</h4></li>

                                        <li class="col-indipls3t2  listyl">
                                            <h4>Corrects Indigestion</h4></li>

                                    </ul>
                                    <ul class="subfnt indctli col-md-12 grey">

                                        <li class="col-indipls3t3  listyl">
                                            <h4>Motion-sickness</h4></li>

                                        <li class="col-indipls3t1   listyl" style="visibility:hidden">
                                            <h4>Corrects Indigestion</h4></li>

                                    </ul>
                                    <div class="col-md-12 tpmrind grey">
                                        <center>
                                            <p class="mntspl">Perfect Ayurvedic Preventive Health Medicine.</p>
                                        </center>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon1">

                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">
                                        <div class="text-left text-justify">
                                            <ul class="subfnt grey lfmarg">
                                                <li class="indctli"><b class="fntwt">Wound healing action </b>
                                                    <p><i class="fntwt">Haridra</i> being strong anti- imflammatory, best anti-septic works effectively in wound healing process. Curcumin of haridra produced large infiltration of macrophages, neutrophils and fibroblast also , the rate of formation of granulation tissue is increased.</p>
                                                    <p><i class="fntwt">Yashtimadhu</i> acts as strong anti- imflammatory agent by promoting the healing of ulcers of stomach and mouth.</p>
                                                    <p><i class="fntwt">Kantakari</i> shows anti- oxidant , anti- septic, antimicrobial actions by stimulating interleukin – 8 and also increases tensile strength of collagen fibres thus achieving better wound healing results.</p>
                                                </li>
                                                <li class="indctli"><b class="fntwt">Immunomodulatory action </b>
                                                    <p>Free radicals scavenger action shown by haridra tulsi yashtimadhu .Active principle curcumin downregulates the expression of COX2 , LOX,iNOS, Urosolic acid of Tulsi & Glycerhizin ( Yashtimadhu) show immunomodulatory action.</p>

                                                </li>
                                                <li class="indctli"><b class="fntwt">Digestive action</b>
                                                    <p>Gingerol ,Shagol of Ginger shows strong anti- imflammatory action thereby Stimulating digestion , absorption , also relieves constipation and flatulence ,reduces nausea, vomiting ,<b class="fntwt"> motion sickness</b>. Yashtimadhu shows antifungal activity against candida albicans and is also powerful antibacterial agent against Gram positive and Gram Negative bacteria.</p>

                                                </li>
                                                <li class="indctli"><b class="fntwt">Respiratory care</b>
                                                    <p>Oleonolic acid,Urosolic acid, polyphenolic acid of Tulsi has anti allergic & anti inflammatory effect in cough – cold, sore throat, bronchitis, asthama.Yashtimadhu shows anti tussive activity, anti allergic activity , spasmolytic activity thus reducing respiratory disorders.</p>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Patha</td>
                                                        <td>Cissampelospareira</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kutaj</td>
                                                        <td>Holarrhena antidysenterica</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kutki</td>
                                                        <td>Picrorhizakurreoa</td>
                                                        <td>0.24gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Ashwagandha</td>
                                                        <td>Withaniasomnifera</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Guduchi</td>
                                                        <td>Tinosporacordifolia</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Chandrashur</td>
                                                        <td>Lepidium sativum</td>
                                                        <td>0.24gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Khajur</td>
                                                        <td>Phoenix sylvestris</td>
                                                        <td>0.24gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Shatavariswarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadinia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/matruj-hd-pouch.png" class="pochwid" /></center>
                                        <p> 1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> Indications <i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Best Immuno-modelator</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Anti-Allergic, Anti-Bacterial.</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Relives fever & Flu like symptoms</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Anti-Inflammatory</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Pain relief</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Wound Healing Enhancer</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Blood-Purifier</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Corrects Indigestion</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>motion-sickness</h4></li>
                                        </ul>

                                        <div class="col-md-12 top-margin-20">
                                            <center>
                                                <p class="mntspl">Perfect Ayurvedic Preventive Health Medicine.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            
            <section class="page-section"data-animation="slideInUp">
 
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title a1">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="pan-pad head">
                                 Matruj Haldiwala Doodh as an “Immunity Booster”
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class=" panel-body">  <ul class="ulpdsty" >
            <li> Along with patients, their relatives, treating doctors and ICCU care doctors in the hospital deal with all sorts of infections thus “Preventive and Supportve” action of Matruj Haldiwala doodh proves to be beneficial for them.</li>
                 <li> Gingerol, Shogaol from adraka, Glycerrhizin of yashtimadhu , Eugenol, Caryophyllene of tulsi, Curcumin of haridra act as powerful anti oxidant and anti inflammatory agents which prevent infections,viral,fungal,bacterial, boost immune power and reduce fever and flu like symptoms.</li>   
                <li> Poor diet, Chronic stress, toxins in body and bacterial imbalance (dysbiosis which means imbalance between beneficial and harmful species of bacteria) leads to leaky gut syndrome which occurs due to hyperpermeability of intestinal epithelial cells allowing passage of toxins, antigens, bacteria. Resulting in autoimmune disease which can be controlled by Matruj Haldiwala Doodh Mixture.</li>          
                <li>The micronutrients found naturally in HDM like iron , magnesium, Calcium, Vit B1, Vit B2, Vit B3  help the patient in its post – operative status.</li> 
                <li> Curcuminoids , Glycerrhizin , Eugenol, Vrosalic acid found in Tulsi, Haridra , Yashtimadhu help manage pain, adaptogenic action reduces stress both mental as well as physical which is boon to postaperativew patient.</li>
                
             </ul></div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title a1">
                            <a class="collapsed head pan-pad" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                             Matruj Haldiwala Doodh Mixture helpful for Teachers, Orators, Singers
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body"> <ul class="ulpdsty">
            <li> Strain on vocal chords is eased and soothing effect is provided by Haridra , Tulsi, Kantakari.</li>
            <li> Anti Allergic Anti Inflammatory action of Haridra  , Tulsi , Yashtimadhu, Kantakari is a boon for singer as it prevents Sore Throat, Broncho spasm thereby performer can do well.</li> 
           </ul></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title a1">
                            <a class="collapsed head" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Matruj Haldiwala doodh Mixture helpful for Swimmers
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body"> <ul class="ulpdsty">
                <li> The micronutrients naturally found in HDM such as Vit B1, B2, B3 , Ca, Mg prove beneficial for a Swimmer</li>
                 <li> Haridra( Curcumin) , Tulsi( eugenol) urosalie acid, oleonol acid along with Kantakari prove to be effective in respiratory care ,which is helpful for Swimmers.
                   
                        <ul style="list-style-type:none;padding:10px">
                            <li>Anti tussive  activity, imparts positive</li>
                               <li> Anti allergic activity, healthy effect</li>
                                <li>Spasmolytic activity on lungs, and reduces chances of Bronchitis, Asthma, Fever.</li>
                               <li> Reduces cough – cold , controls allergies</li>
                            </ul>
                        </li>
                <li> G + ve  G – ve both anti- bacterial activitivity due to glycerrhizin effective in controlling ENT infections found commonly in swimmers. ( Ref.Shaina kalsi)</li>
                <li>Irritable bowel syndrome ,nausea, vomiting, acidity hearburn due to engulping of chlorine water can be controlled by Yashtimadhu , Ginger, Curcumin there by reducing abdominal pain if any.  </li>
            </ul></div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingfour">
                            <h4 class="panel-title a1">
                            <a class="collapsed head" data-toggle="collapse" data-parent="#accordion1" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                           Matruj Haldiwala Doodh Mixture helpful for Athletes , Sportsmen, Gymgoer
                            </a>
                          </h4>

                        </div>
                        <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                            <div class="panel-body"> <ul class="ulpdsty">
                <li> Immune Power boosted and a strong defence system regulation achieved by HDM due to its immunomodulatory anti inflammatory ,anti allergic ,anti oxidants properties.</li>
                 <li> Tulsi and Curcumin  show best adaptogenic behavior that help reduce stress and increase stamina and endurance thereby helping sportsmen achieve  his goal</li>
                 <li> Anti fungal ,  Anti bacterial, Anti viral ,Anti Microbial action of Haridra , Tulsi, Yashtimadhu, Kantakari, Adraka reduces chances of falling sick.</li>
                <li>Yashtimadhu – glycerhizic acid proves to be reducing muscle cramps and fatigue.</li>
                <li>Micronutrients like Ca, Magnesium , Vit B1, B2 etc. prove to  be added benefits through HDM.</li>
              
            </ul></div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title a1">
                            <a class="collapsed head" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                          Matruj Haldiwala Doodh Mixture helpful for Post-operative Care Patients
                            </a>
                          </h4>

                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body"> <ul class="ulpdsty">
                <li> Micronutrients like iron,mg,ca etc. benefits patients with Post operative with nourishment and health.</li>
                 <li> Anti Septic ,Anti inflammatory action of curcumin of Haridra ,Glycerrhizin of yashtimadhu ,Eugenol of tulsi help recovery faster,by boosting immune power.</li>
                 <li> Adaptogens of HDM reduce stress and anxiety , reduce also pain and fever like symptoms, and calmdown nerves enhancing mood which is essential post surgery.</li>
            </ul></div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title a1">
                            <a class="collapsed head" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Matruj Haldiwala Doodh Mixture helpful for Chemotherapy Patients or Cancer Patients
                            </a>
                          </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">  <ul class="ulpdsty">
                <li> The micronutrients naturally found in HDM such as iron, magnesium, VitB1,B2,B3, Calcium etc. help as nourishment.</li>
                 <li> Patients when succumb to toxic side effects of chemo and radiation the curcuminoids  of Haridra, Eugenol from Tulsi show reduced cardio toxic, reduce hepato toxic properties thereby increasing number of sittings of chemo or radiation.</li>
                 <li> Curcumin of Haridra ,Glycerrhizin of yashtimadhu ,Gingerol of  Adraka are effective in suppressing ascites, control distant metastasis to liver ,intestines, improve liver enzymes thus proving to be  hepato protective agent.</li>
                <li> Anti cancer and Adaptogenic properties of haridra ,tulsi ,Kantakari, yashtimadhu reduce stress, depression,anxiety,(insqminia) sleeplessness and increase longevity of life, and also enhance mood.</li>
            </ul></div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <h4 class="panel-title a1">
                            <a class="collapsed head" data-toggle="collapse" data-parent="#accordion1" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                             Matruj Haldiwala Doodh Mixture helpful For Yoga Practitioners
                            </a>
                          </h4>

                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                            <div class="panel-body"> <ul class="ulpdsty">
                <li> Positive Health effects like, <b>Increased brain derived neurotropic factor levels and Increased magnitude of cortisol awakening response,</b> achieved by yoga , are not only maintained but enhanced by adaptgenic behavior of haridra , tulsi,yashtimadhu.   ( Ref.(Dr.Barucha, USA university, Calfornia )</li>
                 <li> Curcumin ,eugenol,urosolic acid , glycerrhizin are active principles found in haridra , tulsi, yashtimadhu, Kantakari that act as anti oxidants, anti- inflammatory agents, immunomodulators which show anti allergic anti microbial action thus controlling infections and fever like symptoms.</li>
                 <li> Increased endurance and flexibility due to yashtimadhu, haridra , tulsi makes HDM a daily regime to perform yoga smoothly throughout year </li>
                <li>  Gingerol , Shagol, Curcumin, glycerrhizi help stimulate liver, flush out toxins, clean bowels, increase apetite which is boon to yoga performer </li>
                <li> Yoga benefits mind too by reducing depression anxiety and increasing alertness, enhancing mood and <b>increasing mind – body readiness</b> these factors are successfully enhanced and maintained by HDM which are key to happy and healthy well- being.</li>
                <li> HDM being natural source of organic micro-nutrients such as  calcium, magnesium, phosphorous, iron, magnenese, zinc, potassium, sodium Vit B1, Vit B2, VitB3 replenish body and reduce fatigue and muscle cramps.</li>
            </ul></div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <h4 class="panel-title a1">
                            <a class="collapsed head" data-toggle="collapse" data-parent="#accordion1" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                Matruj Haldiwala Doodh Mixture helpful For Blood donating volunteers
                            </a>
                          </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                            <div class="panel-body"><ul class="ulpdsty">
                <li> The micronutrients found naturally in HDM like iron, magnesium , calcium, sodium, potassium, Vit B1, B2 , B3, Phoshorous reduce fatigue, ease muscle cramps if any.</li>
                 <li> HDM helps fast recovery post blood donation by encouraging new RBC formation , due to curcuminoids present in haridra.</li>
                 <li> Haridra , Yashtimadhu , Tulsi act as strong anti oxidants , anti inflammatory agents which prevent infections and fever like symptoms  post blood donation, and maintains blood pressure effectively.</li>
                <li> Detoxification of liver , stimulastion of gallbladder by curcumin, shaogol , gingerol aids digestion, controls nausea, vomiting and increases apetite which proves beneficial post blood donation.</li>
                <li> Eugenol, Urosalic acid from tulsi , glycerrhizin from yashtimadhu, Kantakari all prove helpful in respiratory  care and anti allergic action helps healthy status of person post blood donation.</li>
                <li>  HDM boosts immune power so as to facilitate frequent blood donation whenever required without compromising one’s healthy status.</li>
            </ul></div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingnine">
                            <h4 class="panel-title a1">
                            <a class="collapsed head" data-toggle="collapse" data-parent="#accordion1" href="#collapsenine" aria-expanded="false" aria-controls="collapseEight">
                                Matruj Haldiwala Doodh helpful for daily Travellers , Holiday Travellers and Trekkers
                            </a>
                          </h4>
                        </div>
                        <div id="collapsenine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingnine">
                            <div class="panel-body"><ul class="ulpdsty">
                <li> Curcumin from haridra, Gingerol, Shogaol from Adraka, Glycerrhizin from yashtimadhu, Eugenol for tulsi.</li>
                 <li> Show Strong antiinflammatory anti allergic Antioxidant properties.</li>
                 <li> Control bacterial, Viral, fungal infections due to climate change or allergens in atmosphere.</li>
                <li> Prevent fever flu like symptoms  bronchial asthma  Pneumonia. Thus protecting lungs, increasing stamina,reducing fatigue post trek/ travel.</li>
                <li>Effective in nausea,vomiting ,heartburn, aids digestion ,detoxifies liver.</li>
                <li>  Neurocognitive and adaptogenic behavior of tulsi ,Kantakari ,Haridra reduce stress and enhance mood which help enjoy holiday or trek. </li>
            </ul></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</section>
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                               <div class="col-md-8">
                                   <center>
                                    <div style="display:inline-block"><h2 class="hdfnt heading">Available On Amazon  </h2></div>&nbsp;&nbsp;
                                   <div style="display:inline-block">
                                        <a href="https://www.amazon.in/dp/B07DCM2RMR" target="_blank" title="Click here"><img src="assets/img/amazon-india-logo.png" alt="amazon-india-logo"/></a>
                                    </div>
                                  </center>
                               </div>
                               <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="" class="page-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 top-pad-10">
                            <h2 class="heading bottom-margin-50 hdfnt">Other Products</h2>
                            <center><img src="assets/img/Matruj-icon-flower.png" /></center>

                            <div class="tpflw">

                                <div class="prcts">
                                    <div class="col-md-12">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-pad">
                                            <div class="pr1" onclick="window.location.href = 'pregnancy-care.php'">
                                                <div class="infonw green">
                                                    <p>Pregnancy Care</p>
                                                </div>

                                                <div class="popis1 green">
                                                    <h2 itemprop="name" class="text-center"><a href="pregnancy-care.php" class="text-white">Pregnancy Care</a></h2>
                                                </div>
                                                <img src="assets/img/Products/Matruj-category1.jpg" class="primg" alt="" />
                                            </div>
                                            <a class="subfnt" href="pregnancy-care.php">
                                                <p class="padtx clrtxt2 text-center">Pregnancy Care</p>
                                            </a>

                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-pad">
                                            <div class="pr1" onclick="window.location.href = 'post-pregnancy-care.php'">
                                                <div class="infonw green">
                                                    <p>Post Pregnancy Care</p>
                                                </div>
                                                <div class="popis1 green">
                                                    <h2 itemprop="name" class="text-center"><a href="post-pregnancy-care.php" class="text-white">Post Pregnancy Care</a></h2>
                                                </div>
                                                <img src="assets/img/Products/Matruj-category2.jpg" class="primg" alt="" />
                                            </div>
                                            <a class="subfnt" href="post-pregnancy-care.php">
                                                <p class="padtx clrtxt2 text-center">Post Pregnancy Care</p>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    
        <footer id="footer">
            <div class="footer-widget">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                            <img src="assets/img/matruj-logo-footer.png" class="img-responsive"/>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                        <p class="subfnt">
                        <strong class="paratxt">Factory :</strong></p>
                          <p class="subfnt"> S.No. 70/1/1A,
                       Hissa No. 12/10,  <br />Santoshnagar, Lane No. 6,
                        <br />Near PMT Depot,  Katraj<br /> Pune - 411 046<br />
                             
                          </p>
                            <p class="subfnt"> <a href="tel:+919822028065">  +91 9822028065</a></p>
                        <!-- Email -->
                    
                        <!-- Phone -->
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20 padlfcr">
                      <p class="subfnt">
                        <strong class="paratxt">Corporate Office :</strong></p>
                            <p class="subfnt">48A,Parvati Industrial Estate,  <br />
                            Opposite Adinath Society,  <br />
                            Pune - Satara Road, <br />
                            Pune - 411009
                               
                            </p>
                            <p class="subfnt"> <a href="mailto:sales@matrujayurveda.com"> sales@matrujayurveda.com</a></p>
                         </div>
                        <div class="col-md-1"></div>
                        <div class="col-xs-12 col-sm-6 col-md-2 widget bottom-xs-pad-20">
                        
                            <nav class="subfnt">
                                <ul>
                                    <!-- List Items -->
                                    <li class="btmul">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="about-us.php">About</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="product-page.php">Products</a>
                                    </li>
                                 
                                    <li class="btmul">
                                        <a href="Matruj-Contact.php">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        
                    
                    </div>
                    <div class="row top-margin-10">
                        <div class="col-md-12">
                            <p class="text-center fntadv">Matruj Ayurveda Pharmacy Pvt. Ltd.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer-top -->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <!-- Copyrights -->
                       <div class="black col-md-4 col-sm-5 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt"> All rights reserved by Matruj copyright @ 2024 </span>
                       </div>
                         <div class="black col-md-4 col-sm-3 col-xs-12" style="margin-top:10px;float:left" >
                            <center>
                                
                                  <div class="social">
                                      <a href="https://www.facebook.com/matrujayurveda/" target="_blank"><img src="assets/img/footer/matruj-facebook.png" /></a></div>
                                  <div class="social">
                                      <a href="tel:+919822028065"><img src="assets/img/footer/matruj-whatsapp.png" /></a>
                                  </div>
                               </center>
                       </div>
                       <div class="black col-md-4 col-sm-4 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt">Design and Developed by <a href ="https://www.google.com/maps/place/SKSOFT+Solutions/@18.4382452,73.8840807,17z/data=!3m1!4b1!4m6!3m5!1s0x3bc2eb4c4a7cdf19:0xfe4dfc7009701446!8m2!3d18.4382401!4d73.8866556!16s%2Fg%2F11sqs2pqjv?entry=ttu" target="_blank"> SKSOFT Solutions</a></span>
                       </div>
                </div>
                 </div>
             </div>
        
     </footer>

          <!-- Scripts -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script> 
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script> 
    <!-- Menu jQuery plugin -->
     
    <script type="text/javascript" src="assets/js/hover-dropdown-menu.js"></script> 
    <!-- Menu jQuery Bootstrap Addon --> 
    <script type="text/javascript" src="assets/js/jquery.hover-dropdown-menu-addon.js"></script> 
    <!-- Scroll Top Menu -->
     
    <script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script> 
    <!-- Sticky Menu --> 
    <script type="text/javascript" src="assets/js/jquery.sticky.js"></script> 
    <!-- Bootstrap Validation -->
     
    <script type="text/javascript" src="assets/js/bootstrapValidator.min.js"></script> 
    <!-- Revolution Slider -->
     
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
    <script type="text/javascript" src="assets/js/revolution-custom.js"></script> 
    <!-- Portfolio Filter -->
     
    <script type="text/javascript" src="assets/js/jquery.mixitup.min.js"></script> 
    <!-- Animations -->
     
    <script type="text/javascript" src="assets/js/jquery.appear.js"></script> 
    <script type="text/javascript" src="assets/js/effect.js"></script> 
    <!-- Owl Carousel Slider -->
     
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script> 
    <!-- Pretty Photo Popup -->
     
    <script type="text/javascript" src="assets/js/jquery.prettyPhoto.js"></script> 
    <!-- Parallax BG -->
     
    <script type="text/javascript" src="assets/js/jquery.parallax-1.1.3.js"></script> 
    <!-- Fun Factor / Counter -->
     
    <script type="text/javascript" src="assets/js/jquery.countTo.js"></script> 
    <!-- Twitter Feed -->
     
    <script type="text/javascript" src="assets/js/tweet/carousel.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/scripts.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/tweetie.min.js"></script> 
    <!-- Background Video -->
     
    <script type="text/javascript" src="assets/js/jquery.mb.YTPlayer.js"></script> 
    <!-- Custom Js Code -->
     
    <script type="text/javascript" src="assets/js/custom.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="assets/js/JavaScript.js"></script>
        <script>
            $(document).ready(function () {
                $('body').append('<div id="toTop" class="btn topbk"><span class="glyphicon glyphicon-chevron-up"></span></div>');
                $(window).scroll(function () {
                    if ($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }
                });
                $('#toTop').click(function () {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                });
            });

        </script>
        
        <script>
            $('.nav-tabs-dropdown').each(function (i, elm) {

                $(elm).text($(elm).next('ul').find('li.active a').text());

            });

            $('.nav-tabs-dropdown').on('click', function (e) {

                e.preventDefault();

                $(e.target).toggleClass('open').next('ul').slideToggle();

            });

            $('#nav-tabs-wrapper a[data-toggle="tab"]').on('click', function (e) {

                e.preventDefault();

                $(e.target).closest('ul').hide().prev('a').removeClass('open').text($(this).text());

            });

        </script>


    </div>
    
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E6615337" />
</div></form>

<!-- Visual Studio Browser Link -->
<script type="application/json" id="__browserLink_initializationData">
    {"appName":"Chrome","requestId":"c2a334febc7f4b82a381307864d62854"}
</script>
<script type="text/javascript" src="http://localhost:64002/4bab1ae8702d47b784f5593e2b6ec05b/browserLink" async="async"></script>
<!-- End Browser Link -->

</body>
</html>
