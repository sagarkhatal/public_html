﻿

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <title>Matruj | Jeevan Shatavari Plus</title>

        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />

    <meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/32.png" /><link rel="shortcut icon" href="assets/img/72.png" /><link rel="shortcut icon" href="assets/img/114.png" /><link rel="shortcut icon" href="assets/img/144.png" />
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" /><link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet" /><link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:300,400,500,700,400italic,700italic" /><link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" /><link href="assets/css/hover-dropdown-menu.css" rel="stylesheet" />
        <!-- Icomoon Icons -->
        <link href="assets/css/icons.css" rel="stylesheet" />
        <!-- Revolution Slider -->
        <link href="assets/css/revolution-slider.css" rel="stylesheet" /><link href="assets/rs-plugin/css/settings.css" rel="stylesheet" />
        <!-- Animations -->
        <link href="css/animate.min.css" rel="stylesheet" />
        <!-- Owl Carousel Slider -->
        <link href="assets/css/owl/owl.carousel.css" rel="stylesheet" /><link href="assets/css/owl/owl.theme.css" rel="stylesheet" /><link href="assets/css/owl/owl.transitions.css" rel="stylesheet" />
        <!-- PrettyPhoto Popup -->
        <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
        <!-- Custom Style -->
        <link href="assets/css/style.css" rel="stylesheet" /><link href="assets/css/responsive.css" rel="stylesheet" />
        <!-- Color Scheme -->
        <link href="assets/css/color.css" rel="stylesheet" /><link href="assets/css/StyleSheet.css" rel="stylesheet" />
    <style>
        #toTop {
            position: fixed;
            bottom: 10px;
            right: 10px;
            cursor: pointer;
            display: none;
        }
    </style>
<title>

</title></head>
      <div id="pageloader">
            <div class="loader-item fa fa-spin text-color"></div>
        </div>
        <!-- Top Bar -->
    <div class="transparent-header">
			<!-- Sticky Navbar -->
			<header id="sticker" class="sticky-navigation">			
				<!-- Sticky Menu -->
				<div class="navbar navbar-default navbar-bg-light" role="navigation">
					<div class="sticky-menu relative">
						<div class="container">
							<div class="row">
								<div class="col-md-12 pdlfrt">
									<div class="navbar-header mrlft">
									<!-- Button For Responsive toggle -->
								
									<!-- Logo -->
								
									<a class="navbar-brand" href="index.php">
										<img class="site_logo imgsz" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
                                     <span class="mobtogl" style="" id="menu" onclick="openNav()">&#9776; </span>
									<a class="navbar-brand sticky-logo" href="index.php">
										<img class="site_logo imgsz sktp" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
									</div>
								
									<div class="">
									
										<ul class="nav navbar-nav menufnt" style="margin-top:1%;">
											<!-- Home  Mega Menu -->
											<li>
												<a class="active brdr-right mrt" href="index.php">Home</a>
												
											</li>
										    <li>
												<a class="brdr-right mrt" href="about-us.php">About</a>
												
											</li>
                                       
                                         
                                    	<li class="mega-menu">
                                            <a href="product-page.php"" class="brdr-right mrt">Products &nbsp;<i class="fa fa-angle-down" style="font-size:18px"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                   
                                                    <div class="row">
                                                      
                                                        <div class="col-sm-4" style="border-right:1px solid #d2d2d2">
                                                           
                                                            <div class="">
                                                                <div>
                                                                    <a href="pregnancy-care.php" class="parabt prcr anchr active">Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="post-pregnancy-care.php" id="pprgncr" class="parabt psrcr anchr">Post Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="healthcare.php" id="hc" class="parabt htcr anchr">Health Care</a>
                                                                </div>
                                                             
                                                              
                                                            </div>
                                                        </div>
                                                      
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="prcr1">
                                                               
                                                                <div>
                                                                    <a href="Jeevan-shatavarin.php" id="jvnst" class="parabt grey1 anchr">Jeevan Shatavarin</a>
                                                                </div>
                                                               
                                                              
                                                            </div>

                                                            <div class="psrcr1">
                                                                <div>
                                                                    <a href="jeevan-shatavari-plus.php" id="jvnstplus" class="parabt grey1 anchr">Jeevan Shatavarin Plus</a>
                                                                </div>
                                                            </div>

                                                            <div class="htcr1">
                                                                <div>
                                                                    <a href="haldiwala-doodh.php" id="hwd" class="parabt grey1 anchr">Haldiwala Doodh</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="page-links">
                                          
                                                                <img src="assets/img/menu-imgs/Matruj-pc-menu.jpg" class="prgncr" />
                                                                <img src="assets/img/menu-imgs/Matruj-js-menu.jpg" class="jvnst" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-ppc-menu.jpg" class="pprgncr" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-jsp-menu.jpg" class="jvnstplus" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hc-menu.jpg" class="hc" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hd-menu.jpg" class="hwd" style="display:none"/>
                                                        

                                                            </div>
                                                        </div>
                                                 
                                                    </div>
                                                   
                                                </li>
                                            </ul>
                                        </li>
									
                                          
										
                                            <li>
												<a class="mrt" href="Matruj-Contact.php">Contact</a>
												
											</li>
										
										</ul>
									
									</div>
                                     <div id="mySidenav" class="sidenav">
                                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        <div class="overlay-content">
                                            <a href="index.php" onclick="closeNav()">Home</a>
                                            <a href="about-us.php" onclick="closeNav()">About</a>
                                            <a href="product-page.php" onclick="closeNav()">Products</a>
                                              <a href="Matruj-Contact.php" onclick="closeNav()">Contact</a>
                                        </div>
                                    </div>
									
								</div>
								
							</div>
							
						</div>
					
					</div>
				</div>
			</header>
		</div>
<body>
    <form method="post" action="./jeevan-shatavari-plus.php" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WkSX4MVTr2iJ7Bdxhlwh3Rs+gVdGQGkkKyj+EGkhK4PIRmNEgdl+bIMcc51qGXz7IitLauJyaCij7HgIshE34cM9joRfZgks0ZePiLNX418=" />
</div>

    <div>
        
        <div class="page-wrap">
            <section class="jsphead">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-text">
                                <h1 class="entry-title text-left text-white">Jeevan Shatavarin Plus</h1>
                                <ol class="breadcrumb">
                                    <li><a href="index.php" class="text-white">Home</a></li>
                                    <li class="text-white">Post Pregnancy Care</li>
                                    <li class="active text-white">Jeevan Shatavarin Plus</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="" style="position:relative;padding:75px 0px 0px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 bottom-pad-20">
                            <h2 class="heading hdfnt">Jeevan Shatavari Plus</h2>
                            <center><img src="assets/img/Matruj-icon-flower.png" /></center>
                        </div>
                        <div class="col-md-12">
                            <center><img src="assets/img/jeevan-shatavarin/jeevan-shatavarin-plus.png" /></center>
                        </div>
                    </div>
                </div>
            </section>

            <section class="page-section">
                <div class="col-md-12 deskjevn" id="month1">

                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs">
                                <li class="active subfnt">
                                    <a href="#tab_default_1" data-toggle="tab">
                                    Description
                                    <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_2" data-toggle="tab">
                                    Ingredients <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_3" data-toggle="tab">
                                  Dosage <div class="cir"></div>
                                </a>

                                </li>
                                <li class="subfnt">
                                    <a href="#tab_default_4" data-toggle="tab">
                                Indications<div class="cir"></div>
                                </a>

                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_1">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10 text-left text-justify">
                                            <p class="subfnt grey">
                                                Khajur, shatavari act as rich vitamins, protein , Calcium, iron source for mother and her baby via breastfeeding.
                                            </p>
                                            <p class="subfnt grey">Shatavari-is termed as STANYA in Ayurveda,even todays Researchers have proved that shtavari best <b class="fntwt">Galactogue</b> increases milk yield and stimulates growth of mammary glands too. Shatavari also helps uterus regain its normal shape post delivery.</p>
                                            <p class="subfnt grey">Shatavari also acts as best <b class="fntwt">Anti depressant</b> agent by inhibiting MAO-A& MOA-B and through interaction with adrenergenic, dopaminergic, seretonergic and GABAergic systems.</p>
                                            <p class="subfnt grey">Patha, Guduchi, Chandrashur, kutki, kutaj acts as antipyretic reduce fever like symptoms post delivery also stimulate <b class="fntwt">su-pachya stanya nirmiti</b> means easy to digest milk seceretion through mammary glands.</p>
                                            <p class="subfnt grey">Shatavari, Ashwagandha, khajur act as best post natal tonic giving strength to mother post delivery as they are anti oxidant,immunomodulator hence help build up wear tear of tissues.</p>
                                            <p class="subfnt grey">Mudgaparni, Maashparni , Jeevanti acts as balya <b class="fntwt">(बल्य)</b> , bruhan <b class="fntwt">(बृहंण )</b>, rasayan <b class="fntwt">( रसायन )</b>, Jeevaneeya <b class="fntwt">(जीवनीय )</b> that impart strength and longevity to feeding mother.</p>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_default_2">
                                    <table class="table table-bordered table-responsive subfnt grey">
                                        <thead>
                                            <tr>
                                                <th class="hdtbl">Traditional Name</th>
                                                <th class="hdtbl">Botanical Name</th>
                                                <th class="hdtbl">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Patha</td>
                                                <td>Cissampelospareira</td>
                                                <td>0.24gm</td>
                                            </tr>
                                            <tr>
                                                <td>Kutaj</td>
                                                <td>Holarrhena antidysenterica</td>
                                                <td>0.24gm</td>
                                            </tr>
                                            <tr>
                                                <td>Kutki</td>
                                                <td>Picrorhizakurreoa</td>
                                                <td>0.24gm</td>
                                            </tr>

                                            <tr>
                                                <td>Ashwagandha</td>
                                                <td>Withaniasomnifera</td>
                                                <td>0.24gm</td>
                                            </tr>
                                            <tr>
                                                <td>Guduchi</td>
                                                <td>Tinosporacordifolia</td>
                                                <td>0.24gm</td>
                                            </tr>
                                            <tr>
                                                <td>Chandrashur</td>
                                                <td>Lepidium sativum</td>
                                                <td>0.24gm</td>
                                            </tr>

                                            <tr>
                                                <td>Khajur</td>
                                                <td>Phoenix sylvestris</td>
                                                <td>0.24gm</td>
                                            </tr>

                                            <tr>
                                                <td>Shatavariswarasa</td>
                                                <td>Asparagus racemosus</td>
                                                <td>2.08gm</td>
                                            </tr>

                                            <tr>
                                                <td>Mudgaparni</td>
                                                <td>Phaseolus trilobus</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Maashparni</td>
                                                <td>Teramnus labialis</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Jeevanti</td>
                                                <td>Leptadinia reticulata</td>
                                                <td>0.14gm</td>
                                            </tr>

                                            <tr>
                                                <td>Sugar</td>
                                                <td></td>
                                                <td>S. Qu</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane" id="tab_default_3">
                                    <center><img src="assets/img/plus-pouch.png" /></center>
                                    <p class="subfnt grey top-margin-10">
                                        1 Sachet 2 to 3 times a day or as directed by physician with milk or water.
                                    </p>

                                </div>
                                <div class="tab-pane" id="tab_default_4">
                                    <div class="col-md-12 text-left">
                                        <p class="subfnt grey">
                                            <b class="fntwt"> Jeevan Shatavarin Plus</b> helps in re-establishment of health of the woman post delivery. Increases lactation by stimulating mammary glands. Calms nerves and tones muscles and ligaments post the wear tear during pregnancy and delivery. Helps abdomen (uterus) regain its normal shape post delivery. Increases psychological alertness and mental steadiness.
                                        </p>
                                        <p class="subfnt grey"> <b class="fntwt">Su-pachhya Stanya nirmiti</b> which is easily digested by newborn baby thus preventing milk intolerance.</p>

                                    </div>
                                    <div class="col-md-12 tpmrind grey">
                                        <center>
                                            <p class="mntspl">Postnatal care-useful after/post delivery of baby.</p>
                                        </center>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container mobjevn" id="mon1">

                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">  Description<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body subfnt grey">
                                        <div class="text-left text-justify">
                                            <p class="subfnt grey">
                                                Khajur,shatavari act as rich vitamins, protein, Calcium, iron source for mother and her baby via breastfeeding.
                                            </p>
                                            <p class="subfnt grey">Shatavari-is termed as STANYA in Ayurveda,even todays Researchers have proved that shtavari best <b class="fntwt">Galactogue</b> increases milk yield and stimulates growth of mammary glands too. Shatavari also helps uterus regain its normal shape post delivery.</p>
                                            <p class="subfnt grey">Shatavari also acts as best <b class="fntwt">Anti depressant</b> agent by inhibiting MAO-A& MOA-B and through interaction with adrenergenic, dopaminergic, seretonergic and GABAergic systems.</p>
                                            <p class="subfnt grey">Patha, Guduchi, Chandrashur, kutki, kutaj acts as antipyretic reduce fever like symptoms post delivery also stimulate <b class="fntwt">su-pachya stanya nirmiti</b> means easy to digest milk seceretion through mammary glands.</p>
                                            <p class="subfnt grey">Shatavari,Ashwagandha,khajur act as best post natal tonic giving strength to mother post delivery as they are anti oxidant,immunomodulator hence help build up wear tear of tissues.</p>
                                            <p class="subfnt grey">Mudgaparni, Maashparni , Jeevanti acts as balya <b class="fntwt">(बल्य)</b> , bruhan <b class="fntwt">(बृहंण )</b>, rasayan <b class="fntwt">( रसायन )</b>, Jeevaneeya <b class="fntwt">(जीवनीय )</b> that impart strength and longevity to feeding mother.</p>
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Ingredients<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body subfnt">
                                        <div class="table-responsive">
                                            <table class="table table-bordered subfnt grey">
                                                <thead>
                                                    <tr>
                                                        <th class="hdtbl">Traditional Name</th>
                                                        <th class="hdtbl">Botanical Name</th>
                                                        <th class="hdtbl">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Patha</td>
                                                        <td>Cissampelospareira</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kutaj</td>
                                                        <td>Holarrhena antidysenterica</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kutki</td>
                                                        <td>Picrorhizakurreoa</td>
                                                        <td>0.24gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Ashwagandha</td>
                                                        <td>Withaniasomnifera</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Guduchi</td>
                                                        <td>Tinosporacordifolia</td>
                                                        <td>0.24gm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Chandrashur</td>
                                                        <td>Lepidium sativum</td>
                                                        <td>0.24gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Khajur</td>
                                                        <td>Phoenix sylvestris</td>
                                                        <td>0.24gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Shatavariswarasa</td>
                                                        <td>Asparagus racemosus</td>
                                                        <td>2.08gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Mudgaparni</td>
                                                        <td>Phaseolus trilobus</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Maashparni</td>
                                                        <td>Teramnus labialis</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Jeevanti</td>
                                                        <td>Leptadinia reticulata</td>
                                                        <td>0.14gm</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Sugar</td>
                                                        <td></td>
                                                        <td>S. Qu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Dosage<i class="fa fa-angle-down pull-right"></i></a>
                            </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <center><img src="assets/img/plus-pouch.png" class="pochwid" /></center>
                                        <p> 1 Sachet 2 to 3 times a day or as directed by physician with milk or water.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title subfnt">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> Indications <i class="fa fa-angle-down pull-right"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body subfnt grey">
                                        <ul class="subfnt indctli col-md-12">

                                            <li class="col-xs-12 listyl">
                                                <h4>Initiates Stanyanirmiti,</h4></li>

                                            <li class="col-xs-12 listyl">
                                                <h4>Increases stanya</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Supachyastanya</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Mamsadhatuvardhak</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Mamsavatashamak</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Balya, Rasayan</h4></li>
                                            <li class="col-xs-12 listyl">
                                                <h4>Garbhashayashodhak</h4></li>
                                        </ul>

                                        <div class="col-md-12 top-margin-20">
                                            <center>
                                                <p class="mntspl">Postnatal care-useful after/post delivery of baby.</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <section id="" class="page-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 top-pad-10">
                            <h2 class="heading bottom-margin-50 hdfnt">Other Products</h2>
                            <center><img src="assets/img/Matruj-icon-flower.png" /></center>

                            <div class="tpflw">

                                <div class="prcts">
                                    <div class="col-md-12">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-pad">
                                            <div class="pr1" onclick="window.location.href = 'pregnancy-care.php'">
                                                <div class="infonw green">
                                                    <p>Pregnancy Care</p>
                                                </div>

                                                <div class="popis1 green">
                                                    <h2 itemprop="name" class="text-center"><a href="pregnancy-care.php" class="text-white">Pregnancy Care</a></h2>
                                                </div>
                                                <img src="assets/img/Products/Matruj-category1.jpg" class="primg" alt="" />
                                            </div>
                                            <a class="subfnt" href="pregnancy-care.php">
                                                <p class="padtx clrtxt2 text-center">Pregnancy Care</p>
                                            </a>

                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-pad">
                                            <div class="pr1" onclick="window.location.href = 'healthcare.php'">
                                                <div class="infonw green">
                                                    <p>Health
                                                        <br /> Care</p>
                                                </div>
                                                <div class="popis1 green">
                                                    <h2 itemprop="name" class="text-center"><a href="healthcare.php" class="text-white">Health Care</a></h2>

                                                </div>
                                                <img src="assets/img/Products/Matruj-category03.jpg" class="primg" alt="" />
                                            </div>
                                            <a class=" subfnt" href="healthcare.php">
                                                <p class="padtx clrtxt2 text-center">Health Care</p>
                                            </a>
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    
        <footer id="footer">
            <div class="footer-widget">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                            <img src="assets/img/matruj-logo-footer.png" class="img-responsive"/>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                        <p class="subfnt">
                        <strong class="paratxt">Factory :</strong></p>
                          <p class="subfnt"> S.No. 70/1/1A,
                       Hissa No. 12/10,  <br />Santoshnagar, Lane No. 6,
                        <br />Near PMT Depot,  Katraj<br /> Pune - 411 046<br />
                             
                          </p>
                            <p class="subfnt"> <a href="tel:+919822028065">  +91 9822028065</a></p>
                        <!-- Email -->
                    
                        <!-- Phone -->
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20 padlfcr">
                      <p class="subfnt">
                        <strong class="paratxt">Corporate Office :</strong></p>
                            <p class="subfnt">48A,Parvati Industrial Estate,  <br />
                            Opposite Adinath Society,  <br />
                            Pune - Satara Road, <br />
                            Pune - 411009
                               
                            </p>
                            <p class="subfnt"> <a href="mailto:sales@matrujayurveda.com"> sales@matrujayurveda.com</a></p>
                         </div>
                        <div class="col-md-1"></div>
                        <div class="col-xs-12 col-sm-6 col-md-2 widget bottom-xs-pad-20">
                        
                            <nav class="subfnt">
                                <ul>
                                    <!-- List Items -->
                                    <li class="btmul">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="about-us.php">About</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="product-page.php">Products</a>
                                    </li>
                                 
                                    <li class="btmul">
                                        <a href="Matruj-Contact.php">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        
                    
                    </div>
                    <div class="row top-margin-10">
                        <div class="col-md-12">
                            <p class="text-center fntadv">Matruj Ayurveda Pharmacy Pvt. Ltd.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer-top -->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <!-- Copyrights -->
                       <div class="black col-md-4 col-sm-5 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt"> All rights reserved by Matruj copyright @ 2024 </span>
                       </div>
                         <div class="black col-md-4 col-sm-3 col-xs-12" style="margin-top:10px;float:left" >
                            <center>
                                
                                  <div class="social">
                                      <a href="https://www.facebook.com/matrujayurveda/" target="_blank"><img src="assets/img/footer/matruj-facebook.png" /></a></div>
                                  <div class="social">
                                      <a href="tel:+919822028065"><img src="assets/img/footer/matruj-whatsapp.png" /></a>
                                  </div>
                               </center>
                       </div>
                       <div class="black col-md-4 col-sm-4 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt">Design and Developed by <a href ="https://www.google.com/maps/place/SKSOFT+Solutions/@18.4382452,73.8840807,17z/data=!3m1!4b1!4m6!3m5!1s0x3bc2eb4c4a7cdf19:0xfe4dfc7009701446!8m2!3d18.4382401!4d73.8866556!16s%2Fg%2F11sqs2pqjv?entry=ttu" target="_blank"> SKSOFT Solutions</a></span>
                       </div>
                </div>
                 </div>
             </div>
        
     </footer>

          <!-- Scripts -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script> 
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script> 
    <!-- Menu jQuery plugin -->
     
    <script type="text/javascript" src="assets/js/hover-dropdown-menu.js"></script> 
    <!-- Menu jQuery Bootstrap Addon --> 
    <script type="text/javascript" src="assets/js/jquery.hover-dropdown-menu-addon.js"></script> 
    <!-- Scroll Top Menu -->
     
    <script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script> 
    <!-- Sticky Menu --> 
    <script type="text/javascript" src="assets/js/jquery.sticky.js"></script> 
    <!-- Bootstrap Validation -->
     
    <script type="text/javascript" src="assets/js/bootstrapValidator.min.js"></script> 
    <!-- Revolution Slider -->
     
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
    <script type="text/javascript" src="assets/js/revolution-custom.js"></script> 
    <!-- Portfolio Filter -->
     
    <script type="text/javascript" src="assets/js/jquery.mixitup.min.js"></script> 
    <!-- Animations -->
     
    <script type="text/javascript" src="assets/js/jquery.appear.js"></script> 
    <script type="text/javascript" src="assets/js/effect.js"></script> 
    <!-- Owl Carousel Slider -->
     
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script> 
    <!-- Pretty Photo Popup -->
     
    <script type="text/javascript" src="assets/js/jquery.prettyPhoto.js"></script> 
    <!-- Parallax BG -->
     
    <script type="text/javascript" src="assets/js/jquery.parallax-1.1.3.js"></script> 
    <!-- Fun Factor / Counter -->
     
    <script type="text/javascript" src="assets/js/jquery.countTo.js"></script> 
    <!-- Twitter Feed -->
     
    <script type="text/javascript" src="assets/js/tweet/carousel.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/scripts.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/tweetie.min.js"></script> 
    <!-- Background Video -->
     
    <script type="text/javascript" src="assets/js/jquery.mb.YTPlayer.js"></script> 
    <!-- Custom Js Code -->
     
    <script type="text/javascript" src="assets/js/custom.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="assets/js/JavaScript.js"></script>
        <script>
            $(document).ready(function () {
                $('body').append('<div id="toTop" class="btn topbk"><span class="glyphicon glyphicon-chevron-up"></span></div>');
                $(window).scroll(function () {
                    if ($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }
                });
                $('#toTop').click(function () {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                });
            });

        </script>
        
        <script>
            $('.nav-tabs-dropdown').each(function (i, elm) {

                $(elm).text($(elm).next('ul').find('li.active a').text());

            });

            $('.nav-tabs-dropdown').on('click', function (e) {

                e.preventDefault();

                $(e.target).toggleClass('open').next('ul').slideToggle();

            });

            $('#nav-tabs-wrapper a[data-toggle="tab"]').on('click', function (e) {

                e.preventDefault();

                $(e.target).closest('ul').hide().prev('a').removeClass('open').text($(this).text());

            });

        </script>


    </div>
    
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CF3E97F5" />
</div></form>

<!-- Visual Studio Browser Link -->
<script type="application/json" id="__browserLink_initializationData">
    {"appName":"Chrome","requestId":"6c064dd747b640e0a5e858221e3e02e2"}
</script>
<script type="text/javascript" src="http://localhost:64002/4bab1ae8702d47b784f5593e2b6ec05b/browserLink" async="async"></script>
<!-- End Browser Link -->

</body>
</html>
