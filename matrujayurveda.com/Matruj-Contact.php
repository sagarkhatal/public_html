﻿

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <title>Matruj | Contact Us</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
    <meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/32.png" /><link rel="shortcut icon" href="assets/img/72.png" /><link rel="shortcut icon" href="assets/img/114.png" /><link rel="shortcut icon" href="assets/img/144.png" />
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" /><link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet" /><link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:300,400,500,700,400italic,700italic" /><link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" /><link href="assets/css/hover-dropdown-menu.css" rel="stylesheet" />
        <!-- Icomoon Icons -->
        <link href="assets/css/icons.css" rel="stylesheet" />
        <!-- Revolution Slider -->
        <link href="assets/css/revolution-slider.css" rel="stylesheet" /><link href="assets/rs-plugin/css/settings.css" rel="stylesheet" />
        <!-- Animations -->
        <link href="css/animate.min.css" rel="stylesheet" />
        <!-- Owl Carousel Slider -->
        <link href="assets/css/owl/owl.carousel.css" rel="stylesheet" /><link href="assets/css/owl/owl.theme.css" rel="stylesheet" /><link href="assets/css/owl/owl.transitions.css" rel="stylesheet" />
        <!-- PrettyPhoto Popup -->
        <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
        <!-- Custom Style -->
        <link href="assets/css/style.css" rel="stylesheet" /><link href="assets/css/responsive.css" rel="stylesheet" />
        <!-- Color Scheme -->
        <link href="assets/css/color.css" rel="stylesheet" /><link href="assets/css/StyleSheet.css" rel="stylesheet" />
    <style>
        #toTop {
            position: fixed;
            bottom: 10px;
            right: 10px;
            cursor: pointer;
            display: none;
        }
    </style>
<title>

</title></head>
      <div id="pageloader">
            <div class="loader-item fa fa-spin text-color"></div>
        </div>
        <!-- Top Bar -->
    <div class="transparent-header">
			<!-- Sticky Navbar -->
			<header id="sticker" class="sticky-navigation">			
				<!-- Sticky Menu -->
				<div class="navbar navbar-default navbar-bg-light" role="navigation">
					<div class="sticky-menu relative">
						<div class="container">
							<div class="row">
								<div class="col-md-12 pdlfrt">
									<div class="navbar-header mrlft">
									<!-- Button For Responsive toggle -->
								
									<!-- Logo -->
								
									<a class="navbar-brand" href="index.php">
										<img class="site_logo imgsz" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
                                     <span class="mobtogl" style="" id="menu" onclick="openNav()">&#9776; </span>
									<a class="navbar-brand sticky-logo" href="index.php">
										<img class="site_logo imgsz sktp" alt="Site Logo" src="assets/img/matruj-logo.png" />
									</a>
									</div>
								
									<div class="">
									
										<ul class="nav navbar-nav menufnt" style="margin-top:1%;">
											<!-- Home  Mega Menu -->
											<li>
												<a class="active brdr-right mrt" href="index.php">Home</a>
												
											</li>
										    <li>
												<a class="brdr-right mrt" href="about-us.php">About</a>
												
											</li>
                                       
                                         
                                    	<li class="mega-menu">
                                            <a href="product-page.php"" class="brdr-right mrt">Products &nbsp;<i class="fa fa-angle-down" style="font-size:18px"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                   
                                                    <div class="row">
                                                      
                                                        <div class="col-sm-4" style="border-right:1px solid #d2d2d2">
                                                           
                                                            <div class="">
                                                                <div>
                                                                    <a href="pregnancy-care.php" class="parabt prcr anchr active">Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="post-pregnancy-care.php" id="pprgncr" class="parabt psrcr anchr">Post Pregnancy Care</a>
                                                                </div>
                                                                <div>
                                                                    <a href="healthcare.php" id="hc" class="parabt htcr anchr">Health Care</a>
                                                                </div>
                                                             
                                                              
                                                            </div>
                                                        </div>
                                                      
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="prcr1">
                                                               
                                                                <div>
                                                                    <a href="Jeevan-shatavarin.php" id="jvnst" class="parabt grey1 anchr">Jeevan Shatavarin</a>
                                                                </div>
                                                               
                                                              
                                                            </div>

                                                            <div class="psrcr1">
                                                                <div>
                                                                    <a href="jeevan-shatavari-plus.php" id="jvnstplus" class="parabt grey1 anchr">Jeevan Shatavarin Plus</a>
                                                                </div>
                                                            </div>

                                                            <div class="htcr1">
                                                                <div>
                                                                    <a href="haldiwala-doodh.php" id="hwd" class="parabt grey1 anchr">Haldiwala Doodh</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     
                                                        <div class="col-sm-4">
                                                          
                                                            <div class="page-links">
                                          
                                                                <img src="assets/img/menu-imgs/Matruj-pc-menu.jpg" class="prgncr" />
                                                                <img src="assets/img/menu-imgs/Matruj-js-menu.jpg" class="jvnst" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-ppc-menu.jpg" class="pprgncr" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-jsp-menu.jpg" class="jvnstplus" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hc-menu.jpg" class="hc" style="display:none"/>
                                                                <img src="assets/img/menu-imgs/Matruj-hd-menu.jpg" class="hwd" style="display:none"/>
                                                        

                                                            </div>
                                                        </div>
                                                 
                                                    </div>
                                                   
                                                </li>
                                            </ul>
                                        </li>
									
                                          
										
                                            <li>
												<a class="mrt" href="Matruj-Contact.php">Contact</a>
												
											</li>
										
										</ul>
									
									</div>
                                     <div id="mySidenav" class="sidenav">
                                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        <div class="overlay-content">
                                            <a href="index.php" onclick="closeNav()">Home</a>
                                            <a href="about-us.php" onclick="closeNav()">About</a>
                                            <a href="product-page.php" onclick="closeNav()">Products</a>
                                              <a href="Matruj-Contact.php" onclick="closeNav()">Contact</a>
                                        </div>
                                    </div>
									
								</div>
								
							</div>
							
						</div>
					
					</div>
				</div>
			</header>
		</div>
<body>
    <form method="post" action="./Matruj-Contact.php" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OVVLqJIlir71EW+jF1/m1sq+AxIPREnnvuTCSlq+M7ni0wZjsDDsBxJNbaVDh8YXIAykB6mcWYJMRLGekVIzQBSQmbvqWFUCqV3FV6ptIeA=" />
</div>

    <div>
        
        <div class="page-wrap">
            <section class="cntchead">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-text">
                                <h1 class="entry-title text-left text-white">Contact Us</h1>
                                <ol class="breadcrumb">
                                    <li><a href="index.php" class="text-white">Home</a></li>
                                    <li class="active text-white">Contact Us</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="page-section cnctimg">

                <div class="container ">
                    <form id="form1" name="contact-form" class="" method="post" action="Default.php">
                        <div class="col-md-2 col-sm-2 col-xs-2"></div>
                        <div class="col-md-8 col-sm-8 col-xs-8 col-mb form-arr">
                            <h3 class="heading subfnt text-center" style="color:#6dbf53;font-size:28px">Get in touch</h3>
                            <p class="form-message"></p>
                            <div class="contact-form top-margin-30 ">

                                <div class="row ">
                                    <div class="col-md-2 "></div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input id="name" name="name" class="form-control1 required txtfnt" type="text" placeholder="Full Name" aria-required="true" />
                                            <p id="p1" class="red"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2 "></div>

                                </div>

                                <div class="row ">
                                    <div class="col-md-2 "></div>
                                    <div class="col-md-8">
                                        <div class="form-group input-text">
                                            <input id="email" name="email" class="form-control1 required email txtfnt" type="email" placeholder="Email Id" aria-required="true" />
                                            <p id="p2" class="red"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2 "></div>

                                </div>
                                <div class="row ">
                                    <div class="col-md-2 "></div>
                                    <div class="col-md-8">
                                        <div class="form-group input-text">
                                            <input id="phone" name="phno" class="form-control1 txtfnt" type="number" placeholder="Contact No" aria-required="true" />
                                            <p id="p3" class="red"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2 "></div>
                                </div>

                                <div class="row ">
                                    <div class="col-md-2 "></div>
                                    <div class="col-md-8">

                                        <div class="form-group input-text">
                                            <textarea id="message" name="message" class="form-control required txtfnt" style="font-size:16px" placeholder="Message" rows="3" aria-required="true"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>

                                </div>
                                <center>
                                    <button class="btn btn-default subfnt hdtbl" id="submit" value="submit" type="button" name="submit" onclick="DeleteKartItems();">Submit</button>
                                </center>

                                <script type="text/javascript">
                                    //Default.php
                                    function DeleteKartItems() {
                                        debugger;
                                        if (!empty()) {
                                            alert("Please Enter Details")
                                        } else {
                                            var obj = {};
                                            obj.name = $("#name").val();
                                            obj.ToMailid = $("#email").val();
                                            obj.phone = $("#phone").val();
                                            obj.message = $("#message").val();

                                            $.ajax({
                                                type: "POST",
                                                url: 'Default.php/SendItem',
                                                data: JSON.stringify(obj),
                                                contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                success: function(data) {
                                                    //alert('Mail Sent Successfully');
                                                    //alert(msg.returnString);
                                                    if (data.d == "Enquiry Placed - We will notify you soon") {
                                                        clrscr();
                                                    }
                                                    alert(data.d);

                                                },
                                                error: function(e) {
                                                    //alert('Something went wrong, please try again');
                                                    alert(e.Message);
                                                }
                                            });
                                        }
                                    }

                                    function clrscr() {
                                        $("#name").val('');
                                        $("#email").val('');
                                        $("#phone").val('');
                                        $("#message").val('');

                                    };
                                </script>
                                <script type="text/javascript">
                                    function empty() {
                                        debugger;
                                        var x;
                                        var ya = true;

                                        x = $("#name").val();
                                        if (x == "") {
                                            //alert("Please Enter Name");
                                            ya = false;
                                        }
                                        return ya;
                                    };
                                </script>
                            </div>

                        </div>
                        <div class="cntpos">
                            <img src="assets/img/matruj-contact-img.png" class="deskcnt" />
                            <img src="assets/img/matruj-contact-img1.png" class="mobcnt"/>
                        </div>
                    </form>
                </div>

            </section>
        </div>
    
        <footer id="footer">
            <div class="footer-widget">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                            <img src="assets/img/matruj-logo-footer.png" class="img-responsive"/>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                        <p class="subfnt">
                        <strong class="paratxt">Factory :</strong></p>
                          <p class="subfnt"> S.No. 70/1/1A,
                       Hissa No. 12/10,  <br />Santoshnagar, Lane No. 6,
                        <br />Near PMT Depot,  Katraj<br /> Pune - 411 046<br />
                             
                          </p>
                            <p class="subfnt"> <a href="tel:+919822028065">  +91 9822028065</a></p>
                        <!-- Email -->
                    
                        <!-- Phone -->
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20 padlfcr">
                      <p class="subfnt">
                        <strong class="paratxt">Corporate Office :</strong></p>
                            <p class="subfnt">48A,Parvati Industrial Estate,  <br />
                            Opposite Adinath Society,  <br />
                            Pune - Satara Road, <br />
                            Pune - 411009
                               
                            </p>
                            <p class="subfnt"> <a href="mailto:sales@matrujayurveda.com"> sales@matrujayurveda.com</a></p>
                         </div>
                        <div class="col-md-1"></div>
                        <div class="col-xs-12 col-sm-6 col-md-2 widget bottom-xs-pad-20">
                        
                            <nav class="subfnt">
                                <ul>
                                    <!-- List Items -->
                                    <li class="btmul">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="about-us.php">About</a>
                                    </li>
                                    <li class="btmul">
                                        <a href="product-page.php">Products</a>
                                    </li>
                                 
                                    <li class="btmul">
                                        <a href="Matruj-Contact.php">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        
                    
                    </div>
                    <div class="row top-margin-10">
                        <div class="col-md-12">
                            <p class="text-center fntadv">Matruj Ayurveda Pharmacy Pvt. Ltd.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer-top -->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <!-- Copyrights -->
                       <div class="black col-md-4 col-sm-5 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt"> All rights reserved by Matruj copyright @ 2024 </span>
                       </div>
                         <div class="black col-md-4 col-sm-3 col-xs-12" style="margin-top:10px;float:left" >
                            <center>
                                
                                  <div class="social">
                                      <a href="https://www.facebook.com/matrujayurveda/" target="_blank"><img src="assets/img/footer/matruj-facebook.png" /></a></div>
                                  <div class="social">
                                      <a href="tel:+919822028065"><img src="assets/img/footer/matruj-whatsapp.png" /></a>
                                  </div>
                               </center>
                       </div>
                       <div class="black col-md-4 col-sm-4 col-xs-12" style="margin-top:16px;float:left" >
                           <span class="ftrtxt subfnt">Design and Developed by <a href ="https://www.google.com/maps/place/SKSOFT+Solutions/@18.4382452,73.8840807,17z/data=!3m1!4b1!4m6!3m5!1s0x3bc2eb4c4a7cdf19:0xfe4dfc7009701446!8m2!3d18.4382401!4d73.8866556!16s%2Fg%2F11sqs2pqjv?entry=ttu" target="_blank"> SKSOFT Solutions</a></span>
                       </div>
                </div>
                 </div>
             </div>
        
     </footer>

          <!-- Scripts -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script> 
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script> 
    <!-- Menu jQuery plugin -->
     
    <script type="text/javascript" src="assets/js/hover-dropdown-menu.js"></script> 
    <!-- Menu jQuery Bootstrap Addon --> 
    <script type="text/javascript" src="assets/js/jquery.hover-dropdown-menu-addon.js"></script> 
    <!-- Scroll Top Menu -->
     
    <script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script> 
    <!-- Sticky Menu --> 
    <script type="text/javascript" src="assets/js/jquery.sticky.js"></script> 
    <!-- Bootstrap Validation -->
     
    <script type="text/javascript" src="assets/js/bootstrapValidator.min.js"></script> 
    <!-- Revolution Slider -->
     
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
    <script type="text/javascript" src="assets/js/revolution-custom.js"></script> 
    <!-- Portfolio Filter -->
     
    <script type="text/javascript" src="assets/js/jquery.mixitup.min.js"></script> 
    <!-- Animations -->
     
    <script type="text/javascript" src="assets/js/jquery.appear.js"></script> 
    <script type="text/javascript" src="assets/js/effect.js"></script> 
    <!-- Owl Carousel Slider -->
     
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script> 
    <!-- Pretty Photo Popup -->
     
    <script type="text/javascript" src="assets/js/jquery.prettyPhoto.js"></script> 
    <!-- Parallax BG -->
     
    <script type="text/javascript" src="assets/js/jquery.parallax-1.1.3.js"></script> 
    <!-- Fun Factor / Counter -->
     
    <script type="text/javascript" src="assets/js/jquery.countTo.js"></script> 
    <!-- Twitter Feed -->
     
    <script type="text/javascript" src="assets/js/tweet/carousel.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/scripts.js"></script> 
    <script type="text/javascript" src="assets/js/tweet/tweetie.min.js"></script> 
    <!-- Background Video -->
     
    <script type="text/javascript" src="assets/js/jquery.mb.YTPlayer.js"></script> 
    <!-- Custom Js Code -->
     
    <script type="text/javascript" src="assets/js/custom.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="assets/js/JavaScript.js"></script>
        <script>
            $(document).ready(function () {
                $('body').append('<div id="toTop" class="btn topbk"><span class="glyphicon glyphicon-chevron-up"></span></div>');
                $(window).scroll(function () {
                    if ($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }
                });
                $('#toTop').click(function () {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                });
            });

        </script>
        
        <script>
            $('.nav-tabs-dropdown').each(function (i, elm) {

                $(elm).text($(elm).next('ul').find('li.active a').text());

            });

            $('.nav-tabs-dropdown').on('click', function (e) {

                e.preventDefault();

                $(e.target).toggleClass('open').next('ul').slideToggle();

            });

            $('#nav-tabs-wrapper a[data-toggle="tab"]').on('click', function (e) {

                e.preventDefault();

                $(e.target).closest('ul').hide().prev('a').removeClass('open').text($(this).text());

            });

        </script>


    </div>
    
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A536120F" />
</div></form>

<!-- Visual Studio Browser Link -->
<script type="application/json" id="__browserLink_initializationData">
    {"appName":"Chrome","requestId":"7619c33e7dc94fa9be69b875e2164802"}
</script>
<script type="text/javascript" src="http://localhost:64002/4bab1ae8702d47b784f5593e2b6ec05b/browserLink" async="async"></script>
<!-- End Browser Link -->

</body>
</html>
