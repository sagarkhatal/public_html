<?php

    include_once "../config/dbconnect.php";
    session_start();
        
    $user_id=$_SESSION['user_id'];
    if($user_id){
        if(isset($_POST['addToCart']))
        {
            $id=$_POST['id'];
            // $Size=$_POST['size'];
            $unitprice=$_POST['price'];

            // $q = "select * from product_size_variation where product_id=$id AND size_id=$Size";
            // $query=mysqli_query($conn,$q);

            // while($result=mysqli_fetch_array($query))
            // {
            //     $variation_id=$result['variation_id'];
            // }

           // Check if the product_id and user_id combination exists in the cart
            $checkQuery = "SELECT * FROM cart WHERE user_id = $user_id AND product_id = $id";
            $checkResult = mysqli_query($conn, $checkQuery);

            if (mysqli_num_rows($checkResult) > 0) {
                // If the product exists, update the quantity
                $updateQuery = "UPDATE cart SET quantity = quantity + 1 WHERE user_id = $user_id AND product_id = $id";
                $updateResult = mysqli_query($conn, $updateQuery);
                if ($updateResult) {
                    header('Location: ../index.php?cart=updated');
                } else {
                    header('Location: ../index.php?cart=error');
                    echo mysqli_error($conn);
                }
            } else {
                // If the product does not exist, insert a new record
                $insertQuery = "INSERT INTO cart (user_id, quantity, price, variation_id, product_id) VALUES ($user_id, 1, $unitprice, 0, $id)";
                $insertResult = mysqli_query($conn, $insertQuery);
                if ($insertResult) {
                    header('Location: ../index.php?cart=success');
                } else {
                    header('Location: ../index.php?cart=error');
                    echo mysqli_error($conn);
                }
            }
        }
       
   }
   else{
        header('location: ../login.php?error=cart');
   }

?>