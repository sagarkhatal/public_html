<?php

include_once "../config/dbconnect.php";
session_start();

$user_id = $_SESSION['user_id'];
if ($user_id) {
    if (isset($_POST['orderConfirm'])) {
        $name = $_POST['username'];
        $address = $_POST['address'];
        $contact = $_POST['contact'];
        $note = $_POST['note'];
        $pay = $_POST['pay'];

        if (empty($note)) {
            $note = "";
        }

        $insert = " INSERT into orders (user_id, delivered_to, phone_no, address, pay_method, pay_status, order_note) 
            values($user_id,'$name',$contact, '$address','$pay',0,'$note');";
        $Query = mysqli_query($conn, $insert);

        $last_id = mysqli_insert_id($conn);
        if ($Query) {

            $qry = "select * from cart where user_id=$user_id";
            $query = mysqli_query($conn, $qry);

            while ($result = mysqli_fetch_array($query)) {
                $variation_id = $result['variation_id'];
                $product_id = $result['product_id'];
                $quantity = $result['quantity'];
                $price = $result['price'];

                $insert2 = " INSERT into order_details (order_id, variation_id,quantity, price,product_id) 
                    values($last_id,$variation_id,$quantity, $price,$product_id)";
                // echo $insert2;
                // alert("");
                $Query2 = mysqli_query($conn, $insert2);
                if ($Query2) {
                    $qry2 = "DELETE from cart where user_id=$user_id";
                    $query2 = mysqli_query($conn, $qry2);

                    // $qry3="SELECT stock_quantity from product_size_variation where variation_id = $variation_id";
                    // $st = mysqli_query($conn,$qry3);

                    // while($res2 = mysqli_fetch_array($st))
                    // {
                    //     $sq=$res2['stock_quantity'];
                    // }
                    // $finalq=$sq-$quantity;

                    // $query3=mysqli_query($conn,"UPDATE product_size_variation Set stock_quantity=$finalq where variation_id = $variation_id");

                } else {
                    echo mysqli_error($conn);
                }

            }

            header('location: ../orderSuccess.php');
        } else {
            // header('location: ../index.php?order=error');
            echo mysqli_error($conn);
        }
    } else if (isset($_POST['payment-button'])) {
        ?>
            <!-- Import the CCAvenue Crypto file -->
        <?php include '../ccav/Crypto.php' ?>


            <!-- Prepare data for CCAvenue -->
            <?php
            
            $qry = "select * from cart where user_id=$user_id";
            $query = mysqli_query($conn, $qry);
            $total=0;
            while ($result = mysqli_fetch_array($query)) {
                $price = $result['price'];
                $total=$total+$price;
            }
            $name=$_POST['username'];
            $address=$_POST['address'];
            $contact=$_POST['contact'];            
            $note=$_POST['note'];                       
            $pay=$_POST['pay'];
            
            mb_internal_encoding("UTF-8");
            $scheme = $_SERVER['REQUEST_SCHEME'];  // http or https
            $host = $_SERVER['HTTP_HOST'];  // domain name or IP address
            $script_path = dirname($_SERVER['SCRIPT_NAME']);  // directory containing the script
    
            $base_url = $scheme . "://" . $host;
            // Collect required information for payment
            $order_id = rand(10000000, 99999999); // Replace with dynamic order ID
            $amount = $total; // Replace with dynamic amount
    
            // CCAvenue credentials
            $merchant_data = '';
            $working_key = 'C121EF025BB07AD8865EF7722A6409E8';
            $access_code = 'AVHX04KH82CM50XHMC';
            $merchant_id = '2808107';

            // Create an array with all the required information
            $merchant_data = [
                'merchant_id' => $merchant_id,
                'order_id' => $order_id,
                'amount' => $amount,
                'delivery_name' => $name,
                'delivery_address' => $address,
                'delivery_tel' => $contact,
                'merchant_param1' => $user_id,
                'merchant_param2' => $note,
                'merchant_param3' => $pay,
                'currency' => 'INR',
                'redirect_url' => $base_url . '/ccav/ccavResponseHandler.php',
                'cancel_url' => $base_url . '/ccav/ccavResponseHandler.php'
            ];
            
            $merchant_data_encoded = http_build_query($merchant_data);
            $encrypted_data = encrypt($merchant_data_encoded, $working_key);

            ?>

            <!-- HTML form for CCAvenue -->
            <form method="post" name="redirect"
                action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction">
                <input type="hidden" name="encRequest" value="<?php echo $encrypted_data; ?>">
                <input type="hidden" name="access_code" value="<?php echo $access_code; ?>">
            </form>
            <script type="text/javascript">
                document.redirect.submit();
            </script>
        <?php

    }
} else {
    header('location: ../login.php?error=cart');
}
?>