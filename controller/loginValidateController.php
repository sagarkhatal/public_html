<?php

require '../config/dbconnect.php';

session_start();

if(isset($_POST['login-submit'])) {

    $uidmail = $_POST['usernameemail'];
    $password = $_POST['password'];

    if (empty($uidmail) || empty($password)) {
        header("Location: ../index.php?error=emptyfields");
        exit();
    }
    else {
        $sql = "SELECT * FROM users WHERE username='$uidmail' OR email='$uidmail'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $pwcheck = password_verify($password, $row['user_password']);
            if ($pwcheck == false) {
                header("Location: ../index.php?error=wrongpassword");
                exit();
            }
            else if ($pwcheck == true) {
                $_SESSION['isAdmin'] = $row['isAdmin'];
                $_SESSION['user_id'] = $row['user_id'];
                $_SESSION['username'] = $row['username'];
                $_SESSION['email'] = $row['email'];
                header("Location: ../profile.php?login=success");
                exit();
            }
            else {
                header("Location: ../login.php?error=wrongpassword");
                exit();
            }
        }
        else {
            header("Location: ../login.php?error=nouser");
            exit();
        }
    }
}
else {
    header("Location: ../index.php");
    exit();
}

?>
