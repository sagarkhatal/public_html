
<div class="container py-4">
    <div class="row">
        <div class="col-md-7">
            <?php
            session_start();
            if(isset($_SESSION['user_id']))
            {
            include_once "../config/dbconnect.php";
            $id=$_SESSION['user_id'];
            $sql="SELECT * from cart c, products p where user_id=$id AND  c.product_id=p.product_id";
            $result=$conn-> query($sql);
            $price=0;
            $total=0;
            $count=1;
            $product_id;
            if ($result-> num_rows > 0){
            ?>
            <h2>Checkout</h2>
            <table class="table ">
                <thead>
                <tr>
                    <td class="text-center">S.N.</td>
                    <td class="text-center">Product Image</td>
                    <td class="text-center">Product Name </td>
                    
                    <td class="text-center">Quantity</td>
                    <td class="text-center">Price</td>
                </tr>
                </thead>
                <?php
                
                    while ($row=$result-> fetch_assoc()) {
                    
                ?>
                <tr>
                <td><?=$count?></td>
                <td><img width='150px' height='100px' src='<?=$row["product_image"]?>'></td>
                <td><?=$row["product_name"]?></td>
                
                <td><?=$row["quantity"]?></td>
                <td><?=$row["price"]?></td>
                </tr>
                <?php
                        $count=$count+1; 
                        $price=$row["quantity"]*$row["price"];
                        $total=$total+$price;

                        $product_id=$row["product_id"];
                    }
                    
                    
                ?>
            </table>
            <?php
             }
            }
            ?>
        </div>
        <div class="col-md-5">
            <div class="card-account card-container py-5">
                <form class="form-signin" action="./controller/confirmOrderController.php" method="post">
                    
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Delivered to" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="address" class="form-control" placeholder="Address" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="number" name="contact" class="form-control" placeholder="Contact Number" required>
                    </div>
                    <div class="form-group">
                        <textarea name="note" class="form-control" placeholder="Order Note (if any)"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Total Price: Rs. <?= $total ?></label>
                    </div>
                    <div class="form-group">
                        <label>SELECT PAYMENT METHOD<label>
                    </div>
                    
                    <div class="form-group">
                        <input type="radio" id="hide" name="pay" value="Cash" required> Cash on Delivery <br>  
                        <input type="radio"  id="show" name="pay" value="CCAvenue" required> CCAvenue <br>  
                    </div>
                    <button class="btn btn-primary"  style="height:40px; display:none;" type="submit" id="by-cash" name="orderConfirm">Confirm Order</button>
                 
                   </form><!-- /form -->
                   
<!-- Import the CCAvenue Crypto file -->
<?php include 'Crypto.php' ?>


<!-- Prepare data for CCAvenue -->
<?php
    mb_internal_encoding("UTF-8");
    $scheme = $_SERVER['REQUEST_SCHEME'];  // http or https
    $host = $_SERVER['HTTP_HOST'];  // domain name or IP address
    $script_path = dirname($_SERVER['SCRIPT_NAME']);  // directory containing the script
    
    $base_url = $scheme . "://" . $host;
    // Collect required information for payment
    $order_id = rand(10000000, 99999999); // Replace with dynamic order ID
    $amount = $total; // Replace with dynamic amount

    // CCAvenue credentials
    $merchant_data = '';
    $working_key = 'C121EF025BB07AD8865EF7722A6409E8';
    $access_code = 'AVHX04KH82CM50XHMC';
    $merchant_id = '2808107';

    // Create an array with all the required information
    $merchant_data = [
    'merchant_id' => $merchant_id,
    'order_id' => $order_id,
    'amount' => $amount,
    'delivery_name' => $username,
    'delivery_address' => $address,
    'delivery_tel' => $contact,
    'currency' => 'INR',
    'redirect_url' => $base_url . '/ccav/ccavResponseHandler.php',
    'cancel_url' => $base_url . '/ccav/ccavResponseHandler.php'
    ];

    $merchant_data_encoded = http_build_query($merchant_data);
    $encrypted_data = encrypt($merchant_data_encoded, $working_key);
   
?>

<!-- HTML form for CCAvenue -->
<form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction">
    <input type="hidden" name="encRequest" value="<?php echo $encrypted_data; ?>">
    <input type="hidden" name="access_code" value="<?php echo $access_code; ?>">
</form>

<!-- JavaScript to submit the form when the payment button is clicked -->
<script type="text/javascript">
    document.getElementById('payment-button').addEventListener('click', function() {
        document.redirect.submit();
    });
</script>
<button class="btn btn-primary" style="height:40px; display:none;" id="payment-button">Pay with CCAvenue</button>
     
   
    <script>
       

        var cash = document.getElementById("by-cash");
        var btn = document.getElementById("payment-button");
        function handleRadioClick() {
        if (document.getElementById('show').checked) {
            btn.style.display = 'block';
            cash.style.display = 'none';
        } else if(document.getElementById('hide').checked){
            btn.style.display = 'none';
            cash.style.display='block';
        }
        }

        const radioButtons = document.querySelectorAll('input[name="pay"]');
        radioButtons.forEach(radio => {
        radio.addEventListener('click', handleRadioClick);
        });
    </script>
    
            </div><!-- /container -->
        
        </div><!-- /col-md-5 -->
    </div><!-- /row -->
</div>