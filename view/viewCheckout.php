<div class="container py-4">
    <div class="row">
        <div class="col-md-7">
            <?php
            session_start();
            if(isset($_SESSION['user_id'])) {
                include_once "../config/dbconnect.php";
                $id = $_SESSION['user_id'];
                $sql = "SELECT * from cart c, products p where user_id=$id AND c.product_id=p.product_id";
                $result = $conn->query($sql);
                $price = 0;
                $total = 0;
                $count = 1;
                if ($result->num_rows > 0) {
            ?>
            <h2>Checkout</h2>
            <table class="table ">
                <thead>
                <tr>
                    <td class="text-center">S.N.</td>
                    <td class="text-center">Product Image</td>
                    <td class="text-center">Product Name</td>
                    <td class="text-center">Quantity</td>
                    <td class="text-center">Price</td>
                </tr>
                </thead>
                <?php
                    while ($row = $result->fetch_assoc()) {
                ?>
                <tr>
                <td><?= $count ?></td>
                <td><img width='150px' height='100px' src='<?= $row["product_image"] ?>'></td>
                <td><?= $row["product_name"] ?></td>
                <td><?= $row["quantity"] ?></td>
                <td><?= $row["price"] ?></td>
                </tr>
                <?php
                        $count = $count + 1;
                        $price = $row["quantity"] * $row["price"];
                        $total = $total + $price;
                    }
                ?>
            </table>
            <?php
                }
            }
            ?>
        </div>
        <div class="col-md-5">
            <div class="card-account card-container py-5">
                <!-- Form action updated for server-side processing -->
                <form class="form-signin" action="./controller/confirmOrderController.php" method="post">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Delivered to" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="text" name="address" class="form-control" placeholder="Address" required>
                    </div>
                    <div class="form-group">
                        <input type="number" name="contact" class="form-control" placeholder="Contact Number" required>
                    </div>
                    <div class="form-group">
                        <textarea name="note" class="form-control" placeholder="Order Note (if any)"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Total Price: Rs. <?= $total ?></label>
                    </div>
                    <div class="form-group">
                        <label>SELECT PAYMENT METHOD<label>
                    </div>
                    <div class="form-group">
                        <input type="radio" id="hide" name="pay" value="Cash" required> Cash on Delivery <br>  
                        <input type="radio" id="show" name="pay" value="CCAvenue" required> CCAvenue <br>  
                    </div>
                    <button class="btn btn-primary" type="submit" id="by-cash" name="orderConfirm" style="display:none;">Confirm Order</button>
                    <button class="btn btn-primary" style="height:40px; display:none;" name="payment-button"  id="payment-button">Pay with CCAvenue</button>
                </form>
                <!-- Script for handling payment method selection -->
                <script>
                    var cash = document.getElementById("by-cash");
                    var btn = document.getElementById("payment-button");
                    function handleRadioClick() {
                        if (document.getElementById('show').checked) {
                            btn.style.display = 'block';
                            cash.style.display = 'none';
                        } else if (document.getElementById('hide').checked) {
                            btn.style.display = 'none';
                            cash.style.display = 'block';
                        }
                    }
                    const radioButtons = document.querySelectorAll('input[name="pay"]');
                    radioButtons.forEach(radio => {
                        radio.addEventListener('click', handleRadioClick);
                    });
                </script>
            </div>
        </div>
    </div>
</div>
