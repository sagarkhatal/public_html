<div class="container-fluid px-0">
    <img src="./assets/images/bg.jpg" height="400" width="100%" style="object-fit:cover;"/>
    <h1 class="text-center" style="margin-top:-250px; color:#fff; ">About Us</h1>
</div>
<div class="container py-5"></div>
<div class="container-fluid py-5 px-5" style="background-color:#fff; position:relative; margin-top:-3px; border-top-left-radius:50px;border-top-right-radius:50px;">
    <div class="row py-3" >
        <div class="col-6 px-5" style=" min-width:24rem;">
            <img class="float-right" src="./assets/images/about.jpg" alt="Who We Are" width="100%" />
        </div>
        <div class="col-6 px-5" style=" min-width:24rem;">
            <h3>Who We Are</h3>
            <p class="text-justify">
            We, at Matruj Ayurveda dedicatedly focus on giving the best ayurvedic health supplements to the society. We cater to Pregnancy care, Post pregnancy care & Healthcare. Our Practise is based on Principles of Ayurveda that has a tradition that speaks of rich glorious record in the field of Ayurveda. We are blessed with a panel of experienced and trained doctors.
            </br></br>
            Matruj Ayurveda, started 15 years ago as a small manufacturing unit, an extended wing of ayurveda clinic, where doctors came together as young professionals. Gradually their dream shaped up towards a noble aim to provide quality medicines to the masses.
            </br></br>
            Matruj Ayurveda Pharmacy is dedicated to the “Mother” health care, right from the preconception to the post delivery stages & “Child” health care, right from fertilization to childhood stages.
        
            </p>
        </div>

    </div>
</div>