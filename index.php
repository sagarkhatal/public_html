
<!DOCTYPE html>
<html lang="en">
    <head><meta charset="utf-8">
        
        <meta name="author" content="Bigyan Bista, Sushant Rahapal, Susant Basnet">
        <meta name="keywords" content="matruj ayurveda">
        <meta name="description" content="matruj ayurveda">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./assets/css/style.css"></link>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Matruj Ayurveda</title>
        <script src="https://khalti.s3.ap-south-1.amazonaws.com/KPG/dist/2020.12.17.0.0.0/khalti-checkout.iffe.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
        <script>function showmsg(msg, color = "green") {
            Toastify({
                text: msg,
                duration: 3000, // 3 seconds
                gravity: "top", // toast position
                position: "center", // toast position
                backgroundColor: color, // toast color
            }).showToast(); // show the toast
        }
    </script>

    </head>
    <body>
        <?php 
        if(isset($_SESSION['isAdmin']) == 1){
            include "./adminHeader.php";
        }else{
            include "./header.php";
        }
           
        ?>
        
        <div id="main-content" class="allContent-section ">
        <!-- <h3> <a href="gmail.php">gmail</a></h3> -->
            <?php 
                include_once "./view/viewProduct.php";
               
            ?>
            
        </div>  
        <?php 
            include "./footer.php";

            if (isset($_GET['cart']) && $_GET['cart'] == "success") {
                echo '<script> showmsg("Added to Cart")</script>';

            }else if  (isset($_GET['cart']) && $_GET['cart'] == "updated") {
                echo '<script> showmsg("Updated to Cart")</script>';
            }else if (isset($_GET['cart']) && $_GET['cart'] == "error") {
                echo '<script> showmsg("Already in Cart","red")</script>';
            }

            if (isset($_GET['review']) && $_GET['review'] == "success") {
                echo '<script> showmsg("Review submitted successfully")</script>';
            }else if (isset($_GET['review']) && $_GET['review'] == "error") {
                echo '<script> showmsg("Review submitting fail","red")</script>';
            }
        ?>
        
        
        <script type="text/javascript" src="./assets/js/ajaxWork.js"></script>  
        
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" ></script>
      

        
    </body>
</html>